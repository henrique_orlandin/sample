<?php
    $installments = 0;
    $installmentPrice = 0;
    for ($i = 2; $i <= MAX_INSTALLMENTS; $i++){
        if ($productPrice / $i > MIN_INSTALLMENT_PRICE){
            $installments = $i;
            $installmentPrice = mysql_decimal_to_number(bcdiv($productPrice, $i, 2));
        }
    }

    $debtPrice = $productPrice - ((DEBT_PRICE_DISCOUNT * $productPrice) / 100);
?>
<?php if ($installments > 1){ ?>
    <p>
        Em até <strong><?php echo $installments; ?>x</strong> de <strong>R$<?php echo $installmentPrice; ?></strong> sem juros
        <?php if ($showMore){ ?><button class="btn-installments" id="btn-installments">ver parcelas</button><?php } ?>
        <br /><strong>R$<?php echo mysql_decimal_to_number($debtPrice); ?></strong> via transferência bancária <?php echo DEBT_PRICE_DISCOUNT ? '(-' . DEBT_PRICE_DISCOUNT . '%)' : ''; ?>
        <?php if ($showMore){ ?><button class="btn-installments" id="btn-debt-account">ver mais</button><?php } ?>
    </p>
<?php } ?>
<?php /*<p>À vista no boleto por <strong>R$ 1490,00</strong> (10% de desconto)</p>*/ ?>