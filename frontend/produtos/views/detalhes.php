<main id="product-details">

    <section class="common-header desktop-only">
        <div class="page-data">
            <div class="icon desktop-only"><img src="<?php echo resize_url('image/resize?w=35&h=35&src='.$category_father->image); ?>" alt="Cordas"></div>
            <div class="text">
                <ul class="breadcrumbs">
                    <li class="breadcrumb-data"><a href="<?php echo site_url('produtos/'.$category_father->slug) ?>"><?php echo $category_father->title; ?></a></li>
                    <li class="breadcrumb-data"><a href="<?php echo site_url('produtos/'.$category->slug) ?>"><?php echo $category->title; ?></a></li>
                    <li class="breadcrumb-data"><strong><?php echo $product->title; ?></strong></li>
                </ul>
            </div>
        </div>
    </section>

    <article>
        <div class="product-info">
            <section class="gallery left">

                <a class="common-back<?php echo $back_url ? ' history-back' : ''; ?>" href="<?php echo site_url('produtos/'.$category->slug); ?>"><?php echo load_svg('back-arrow.svg'); ?></a>
                <?php if($product->original_price != $product->price){
                    $off = round(100 - ((float)$product->price * 100 / (float)$product->original_price));
                ?>
                    <div class="offer">
                        <?php echo load_svg('arrow-down.svg'); ?>
                        <span class="save"><?php echo $off; ?>%<br/>off</span>
                    </div>
                <?php } ?>

                <div class="main-gallery">
                    <ul class="gallery-list">
                        <?php foreach($product->gallery as $gallery){ ?>
                            <li class="gallery-data">
                                <?php
                                echo lazyload(array(
                                    'src'   => resize_url('image/resize?w=760&h=460&src='.$gallery->path),
                                    'alt'   => $gallery->subtitle,
                                    'rel'   => 'gallery',
                                    'class' => 'lazyload image colorbox',
                                    'tag'   => 'a',
                                    'href'  => resize_url('image/resize?w=1250&h=900&src='.$gallery->path),
                                    'data-autoload' => false,
                                )); ?>
                            </li>
                        <?php } ?>
                    </ul>
                    <div class="common-dots mobile-only"></div>
                </div>

                <div class="small-gallery desktop-only">
                    <ul class="gallery-list">
                        <?php foreach($product->gallery as $gallery){ ?>
                            <li class="gallery-data">
                                <?php
                                echo lazyload(array(
                                    'src'   => resize_url('image/resize?w=80&h=80&src='.$gallery->path),
                                    'alt'   => $gallery->subtitle,
                                    'class' => 'lazyload image',
                                    'tag'   => 'figure',
                                    'data-autoload' => false,
                                )); ?>
                            </li>
                        <?php } ?>
                    </ul>
                    <div class="common-arrows">
                        <button class="arrow prev"><i class="fa fa-angle-left"></i></button>
                        <button class="arrow next"><i class="fa fa-angle-right"></i></button>
                    </div>
                </div>

            </section>

            <section class="sell-data right">

                <h1 class="title"><?php echo $product->title; ?></h1>
                <?php if(!empty($product->code)){ ?>
                    <p class="code">Código <?php echo $product->code; ?></p>
                <?php } ?>

                <div class="rating">
                    <div class="common-rating">
                        <?php for ($i = 1; $i < 6; $i++) { ?>
                            <span class="star <?php echo $i <= ceil($product->review_total) ? 'full':''; ?>"><?php echo load_svg('star.svg'); ?></span>
                        <?php } ?>
                    </div>
                    <button id="show-reviews" class="show-reviews"><?php echo count($product->review); ?> avaliações</button>
                </div>

                <?php if($product->inventory > 0) { ?>

                    <div id="price-content"><?php
                        if (!$product->has_variation) {
                            $this->load->view('_detalhes_valor');
                        } else {
                        ?>
                        <div class="price">
                            <small class="old no-strike">A partir de</small>
                            <strong class="current">R$ <span id="product-price"><?php echo mysql_decimal_to_number($product->min_price); ?></span></strong>
                        </div>
                        <div class="payment">
                            <p>Selecione abaixo a opção desejada:</p>
                        </div>

                        <?php
                        }
                    ?></div>

                    <form action="<?php echo site_url('carrinho/add'); ?>" method="POST" id="cart-add" class="common-form">

                        <?php if(!empty($product->variations)){ ?>
                            <ul class="variations-list">
                                <li class="variation-data">
                                    <label for="variation">Opcional</label>
                                    <div class="form-group">
                                        <div class="select-wrapper">
                                            <?php $this->load->view('comum/preloader'); ?>
                                            <select name="variation" id="variation" data-product="<?php echo $product->id; ?>" placeholder="Selecione" required>
                                                <option selected disabled>Selecione</option>
                                                <?php
                                                    foreach($product->variations as $variation){
                                                        if($variation->quantity >= 1){ ?>
                                                            <option value="<?php echo $variation->id; ?>"><?php echo $variation->title; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <div class="transport-box">
                            <div class="form-group quantity-box">
                                <label for="quantity">Quant.</label>
                                <input type="number" name="quantity" value="1" id="quantity" required class="mask-quantity">
                            </div>
                            <div class="form-group zipcode-box">
                                <label for="zipcode">Calcular frete e prazo:</label>
                                <input type="<?php echo $_is_mobile ? 'tel' : 'text'; ?>" name="zipcode" placeholder="CEP" id="zipcode">
                                <button type="button" id="calculate" data-product="<?php echo $product->id; ?>" class="common-button">
                                    <span>Calcular</span>
                                    <?php $this->load->view('comum/preloader'); ?>
                                </button>
                            </div>
                            <div class="shipping-result"></div>
                        </div>

                        <input type="hidden" name="product" value="<?php echo $product->id; ?>">
                        <button type="submit" id="add-to-cart" class="common-button<?php echo ($product->has_variation) ? ' disabled' : ''; ?>">
                            <?php echo load_svg('cart.svg'); ?><span>Comprar</span>
                            <?php $this->load->view('comum/preloader'); ?>
                        </button>

                    </form>

                <?php } else { ?>
                <div class="no-stock">
                    <p>Produto Esgotado</p>
                    <a href="<?php echo site_url('contato'); ?>" class="common-button"><span>Solicitar Orçamento</span></a>
                </div>
                <?php } ?>

            </section>
        </div>

        <section class="product-content">

            <?php if(!empty($product->videos) || !empty($product->description)){ ?>
                <div class="left description">
                    <?php if(!empty($product->videos)){ ?>
                        <h2 class="title">Vídeos do produto</h2>
                        <div class="video-gallery">
                            <ul class="gallery-list">
                                <?php foreach ($product->videos as $key => $value) { ?>
                                <li class="gallery-data">
                                    <?php
                                    echo lazyload(array(
                                        'src'   => resize_url('image/resize?w=960&h=350&src=https://img.youtube.com/vi/'.get_youtube_id($value->link).'/0.jpg'),
                                        'alt'   => $product->title,
                                        'rel'   => 'videos',
                                        'class' => 'lazyload iframe colorbox',
                                        'tag'   => 'a',
                                        'href'  => '//www.youtube.com/embed/'.get_youtube_id($value->link),
                                        'data-autoload' => false,
                                    ));
                                    ?>
                                </li>
                                <?php } ?>
                            </ul>
                            <div class="common-arrows desktop-only">
                                <button class="arrow prev"><i class="fa fa-angle-left"></i></button>
                                <button class="arrow next"><i class="fa fa-angle-right"></i></button>
                            </div>
                            <div class="common-dots"></div>
                        </div>
                    <?php } ?>
                    <?php if(trim(strip_tags($product->description))){ ?>
                        <h2 class="title">Descrição do Produto</h2>
                        <div class="common-text">
                            <?php echo $product->description; ?>
                        </div>
                    <?php } ?>

                </div>
            <?php } ?>

            <?php if(!empty($product->characteristics)){ ?>
                <div class="right characteristcs">
                    <h2 class="title">Características Técnicas</h2>
                    <div class="common-text">
                        <ul>
                            <?php foreach ($product->characteristics as $characteristic) { ?>
                                <li><?php echo $characteristic->title.' '.$characteristic->value; ?></li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            <?php } ?>

        </section>
    </article>

    <?php if(!empty($product->related)){ ?>
        <aside id="related-products" class="common-products">
            <h2 class="">Você também pode gostar de:</h2>
            <div class="related-gallery">
                <?php $this->load->view('listagem', array('products' => $product->related)); ?>
                <div class="common-arrows desktop-only">
                    <button class="arrow prev"><i class="fa fa-angle-left"></i></button>
                    <button class="arrow next"><i class="fa fa-angle-right"></i></button>
                </div>
                <div class="common-dots mobile-only"></div>
            </div>
        </aside>
    <?php } ?>

    <section class="reviews" id="reviews">

        <h2 class="title">Avaliações</h2>

        <?php if($product->review) { ?>
        <ul class="review-list">
            <?php foreach ($product->review AS $review) { ?>
            <li class="review-data">
                <div class="common-rating">
                    <?php for ($i = 1; $i < 6; $i++) { ?>
                        <span class="star <?php echo $i <= $review->rating ? 'full':''; ?>"><?php echo load_svg('star.svg'); ?></span>
                    <?php } ?>
                </div>
                <div class="common-text">
                    <?php echo $review->comment; ?>
                </div>
                <div class="author">
                    <strong><?php echo $review->name; ?></strong> - <time datetime="<?php echo $review->date; ?>"><?php echo $review->dateFormated; ?></time>
                </div>
            </li>
            <?php } ?>
        </ul>
        <div class="common-dots mobile-only"></div>
        <?php } ?>

        <form method="post" novalidate action="<?php echo site_url('produtos/rating'); ?>" id="rating-form" class="common-form ajax-form">
            <input type="hidden" name="product" value="<?php echo $product->id; ?>">
            <p class="form-title">Sua Avaliação</p>
            <div class="rating">
                <div class="star">
                    <input type="radio" name="rating" value="1" id="form-rating-1">
                    <label for="form-rating-1"><?php echo load_svg('star.svg'); ?></label>
                </div>
                <div class="star">
                    <input type="radio" name="rating" value="2" id="form-rating-2">
                    <label for="form-rating-2"><?php echo load_svg('star.svg'); ?></label>
                </div>
                <div class="star">
                    <input type="radio" name="rating" value="3" id="form-rating-3">
                    <label for="form-rating-3"><?php echo load_svg('star.svg'); ?></label>
                </div>
                <div class="star">
                    <input type="radio" name="rating" value="4" id="form-rating-4">
                    <label for="form-rating-4"><?php echo load_svg('star.svg'); ?></label>
                </div>
                <div class="star">
                    <input type="radio" name="rating" value="5" id="form-rating-5">
                    <label for="form-rating-5"><?php echo load_svg('star.svg'); ?></label>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <textarea name="comment"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="form-group submit">
                    <button type="submit" class="common-button">
                        <span>Enviar Avaliação</span><?php $this->load->view('comum/preloader'); ?>
                    </button>
                </div>
            </div>
        </form>

    </section>


    <aside class="common-advantages">
        <?php $this->load->view('comum/advantages'); ?>
    </aside>
</main>
