<main id="products" data-url="<?php echo site_url($this->uri->uri_string()); ?>">

    <section class="common-header">

        <?php if(!isset($search)) { ?>
        <div class="page-data">
            <?php   if ($category_father) { ?>
            <div class="icon desktop-only"><img src="<?php echo resize_url('image/resize?w=55&h=55&src='.$category_father->image); ?>" alt="<?php echo $category_father->title; ?>"></div>
            <div class="text">
                <!-- <ul class="breadcrumbs">
                    <li class="breadcrumb-data"><a href="<?php echo site_url('produtos/'.$category_father->slug); ?>"><?php echo $category_father->title; ?></a></li>
                    <li class="breadcrumb-data"><strong><?php echo $category->title; ?></strong></li>
                </ul> -->
                <div class="icon mobile-only"><img src="<?php echo resize_url('image/resize?w=60&h=60&src='.$category_father->image); ?>" alt="<?php echo $category_father->title; ?>"></div>
                <h1 class="title products-list-title">
                    <a href="<?php echo site_url('produtos/'.$category_father->slug); ?>"><?php echo $category_father->title; ?> - </a>
                    <span><?php echo $category->title; ?></span>
                </h1>
            </div>
            <?php   } else { ?>
            <div class="icon desktop-only"><img src="<?php echo base_img('icon/favicon-32x32.png'); ?>" alt="Hotmusic - Produtos"></div>
            <div class="text">
                <div class="icon mobile-only"><img src="<?php echo base_img('icon/favicon-32x32.png'); ?>" alt="Hotmusic - Produtos"></div>
                <h1 class="title products-list-title">
                    <span>Produtos</span>
                </h1>
            </div>
            <?php   } ?>
        </div>
        <?php } else { ?>
        <div class="page-data">
            <div class="icon desktop-only"><?php echo load_svg('search.svg'); ?></div>
            <div class="text results">
                <ul class="breadcrumbs">
                    <li class="breadcrumb-data"><a href="<?php echo site_url(); ?>">Página Incial</a></li>
                    <li class="breadcrumb-data"><strong>Pesquisa</strong></li>
                </ul>
                <div class="icon mobile-only"><?php echo load_svg('search.svg'); ?></div>
                <?php if($total > 0) { ?>
                <div class="title">
                    <?php echo str_pad($total, 2, '0', STR_PAD_LEFT).' '.($total > 1 ? 'Resultados' : 'Resultado'); ?>
                </div>
                <div class="search">Pesquisa por <strong>"<?php echo $search; ?>"</strong></div>
                <?php } else { ?>
                <div class="title">Nenhum Resultado</div>
                <div class="search">Pesquisa por <strong>"<?php echo $search; ?>"</strong></div>
                <?php } ?>
            </div>
        </div>
        <?php } ?>
        <!-- ordanação -->
        <form action="<?php echo site_url($this->uri->uri_string()); ?>" class="common-form order desktop-only" id="order-form" method="get">
            <?php if(isset($search)) { ?>
            <input type="hidden" name="q" value="<?php echo $search; ?>">
            <?php } ?>
            <label for="order" class="label">Ordenação</label>
            <div class="select-wrapper">
                <select class="select" name="order" id="order">
                    <option <?php echo !isset($order_by) || $order_by == '' ? 'selected' : ''; ?> value="">Selecione</option>
                    <option <?php echo isset($order_by) && $order_by == 'main.price ASC' ? 'selected' : ''; ?> value="main.price ASC">Menor Preço</option>
                    <option <?php echo isset($order_by) && $order_by == 'main.price DESC' ? 'selected' : ''; ?> value="main.price DESC">Maior Preço</option>
                    <!-- <option <?php echo isset($order_by) && $order_by == 'best-seller' ? 'selected' : ''; ?> value="best-seller">Mais Vendidos</option> -->
                    <option <?php echo isset($order_by) && $order_by == 'main.title ASC' ? 'selected' : ''; ?> value="main.title ASC">Ordem Alfabética</option>
                </select>
            </div>
        </form>
    </section>

    <div class="container">

        <section class="options-selection mobile-only">
            <?php if (!empty($filters)){ ?>
            <button class="option-button" id="btn-filters"><?php echo load_svg('filter.svg'); ?><span>Filtrar</span></button>
            <?php } ?>
            <button class="option-button" id="btn-order"><?php echo load_svg('order.svg'); ?><span>Ordenar</span></button>
            <ul class="order-list" id="mobile-order">
                <li data-value="" class="order-data <?php echo !isset($order_by) || $order_by == '' ? ' selected' : ''; ?>">Selecione</li>
                <li data-value="main.price ASC" class="order-data <?php echo isset($order_by) && $order_by == 'main.price ASC' ? ' selected' : ''; ?>">Menor Preço</li>
                <li data-value="main.price DESC" class="order-data <?php echo isset($order_by) && $order_by == 'main.price DESC' ? ' selected' : ''; ?>">Maior Preço</li>
                <li data-value="best-seller" class="order-data <?php echo isset($order_by) && $order_by == 'best-seller' ? ' selected' : ''; ?>">Mais Vendidos</li>
                <li data-value="main.title ASC" class="order-data <?php echo isset($order_by) && $order_by == 'main.title ASC' ? ' selected' : ''; ?>">Ordem Alfabética</li>
            </ul>
        </section>

        <!-- filtros de busca de produto -->
        <?php if (!empty($filters)){ ?>
        <section class="filter">
            <h1 class="title">
                <button type="button" id="back-filter" class="mobile-only"><?php echo load_svg('left-arrow.svg'); ?></button>
                <p class="desktop-only">Filtrar <br/>Resultados</p>
                <p class="mobile-only">Refine sua busca</p>
            </h1>
            <form action="<?php echo site_url($this->uri->uri_string()); ?>" class="common-form" id="filter-form" method="get">
                <button type="submit" class="common-button top-button desktop-only">
                    <?php $this->load->view('comum/preloader'); ?>
                    <?php echo load_svg('filter-fill.svg'); ?>
                    <span>Aplicar Filtros</span>
                </button>
                <div class="filters-container">
                    <?php
                    foreach ($filters as $key => $value) {
                        $filter_values = count($value['values']);
                        if($filter_values === 0)
                            continue;
                    ?>
                        <div class="filter-dup">
                            <div class="filter-title">
                                <?php echo ($value['title']); ?>
                                <div class="mobile-icon mobile-only">
                                    <div class="more"><?php echo load_svg('more.svg'); ?></div>
                                    <div class="less"><?php echo load_svg('less.svg'); ?></div>
                                </div>
                            </div>
                            <ul class="filters-list">
                                <?php
                                    foreach($value['values'] as $i => $filter){
                                        if(isset($filter_array[$value['id']]) && ((is_array($filter_array[$value['id']]) && in_array($filter, $filter_array[$value['id']])) || $filter_array[$value['id']] == $filter)){
                                            $checked = TRUE;
                                        }else{
                                            $checked = FALSE;
                                        }
                                        ?>
                                        <li class="common-checkbox">
                                            <input type="checkbox" <?php echo ($checked) ? 'checked' : ''; ?> name="<?php echo $value['id']; ?>[]" id="<?php echo $value['id']; ?>-<?php echo $key; ?>-<?php echo slug($filter); ?>" value="<?php echo $filter; ?>" />
                                            <div class="checkbox"></div>
                                            <label for="<?php echo $value['id']; ?>-<?php echo $key; ?>-<?php echo slug($filter); ?>"><?php echo $filter; ?></label>
                                        </li>
                                        <?php
                                    }
                                ?>
                            </ul>
                            <?php if($filter_values > 3) { ?>
                            <button type="button" class="see-more desktop-only">Ver Mais</button>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
                <button type="submit" class="common-button" >
                    <?php $this->load->view('comum/preloader'); ?>
                    <?php echo load_svg('filter-fill.svg'); ?>
                    <span>Aplicar Filtros</span>
                </button>
            </form>
        </section>
        <?php } ?>

        <!-- listagem de produtos e paginação -->
        <section class="products <?php echo empty($filters) ? 'full' : ''; ?>" id="ajax-content">
            <?php $this->load->view('ajax-content', array('products' => $products)); ?>
        </section>

    </div>

    <aside class="common-advantages">
        <?php $this->load->view('comum/advantages'); ?>
    </aside>
</main>