<?php foreach ($options as $key => $option){ ?>
<div class="shipping-info">
    <strong><?php echo (int) $option->price > 0 ? 'R$ '.mysql_decimal_to_number($option->price) : 'FRETE GRÁTIS'; ?></strong> - <?php echo $option->title . ((int) $option->price > 0 ? ' - Entrega em até ' . $option->days.($option->days == 1 ? ' dia útil' : ' dias úteis') : ''); ?>
</div>
<?php }
if(empty($options)) { ?>
<div class="no-shipping">Nenhuma forma de entrega encontrada. <a href="<?php echo site_url('contato'); ?>">Entre em contato conosco.</a></div>
<?php } ?>