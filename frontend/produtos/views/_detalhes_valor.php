<div class="price">
    <?php if($product->original_price != $product->price){ ?>
        <small class="old">R$ <span id="product-price-old"><?php echo mysql_decimal_to_number($product->original_price); ?></span></small>
    <?php } ?>
    <strong class="current">R$ <span id="product-price"><?php echo mysql_decimal_to_number($product->price); ?></span></strong>
</div>

<div class="payment">
    <?php echo $this->load->view('_parcelas', array(
        'productPrice' => $product->price,
        'showMore' => TRUE
    )); ?>
</div>