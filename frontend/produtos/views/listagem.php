<ul class="products-list">
    <?php foreach($products as $product) { ?>
        <li class="product-data">
            <a href="<?php echo site_url('produtos/'.$product->slug); ?>" class="content">
                <?php
                $options = array(
                    'src'           => resize_url('image/resize_canvas?w=320&h=270&bg=1&src='.$product->image),
                    'alt'           => $product->title,
                    'class'         => 'lazyload',
                    'data-viewport' => 1,
                    'tag'           => 'figure'
                );
                echo lazyload($options);
                ?>
                <h3 class="title"><?php echo $product->title; ?> </h3>
                <!-- <div class="common-rating">
                    <?php for ($i = 1; $i < 6; $i++) { ?>
                        <span class="star <?php echo $i <= ceil($product->review_total) ? 'full':''; ?>"><?php echo load_svg('star.svg'); ?></span>
                    <?php } ?>
                </div> -->
                <?php if(($product->has_variation == '0' && $product->inventory >= 1)) { ?>
                    <div class="price">
                        <?php if($product->original_price != $product->price){ ?>
                            <small class="old">R$<?php echo mysql_decimal_to_number($product->original_price); ?></small>
                        <?php } ?>
                        <strong class="current">R$<?php echo mysql_decimal_to_number($product->price); ?><!--  <small>à vista</small> --></strong>
                    </div>
                    <div class="payment">
                        <?php echo $this->load->view('_parcelas', array(
                            'productPrice' => $product->price,
                            'showMore' => FALSE
                        )); ?>
                    </div>
                    <?php if($product->original_price != $product->price){
                            $off = round(100 - ((float)$product->price * 100 / (float)$product->original_price));
                        ?>
                        <div class="offer">
                            <?php echo load_svg('arrow-down.svg'); ?>
                            <span class="save"><?php echo $off; ?>%<br/>off</span>
                        </div>
                    <?php } ?>
                <?php } else if ($product->has_variation == '0' && $product->inventory == 0){ ?>
                    <div class="no-stock">
                        <p>Esgotado</p>
                        <button data-href="<?php echo site_url('contato'); ?>" class="common-button"><span>Solicitar Orçamento</span></button>
                    </div>
                <?php } else if ($product->has_variation == '1' && $product->inventory >= 1) { ?>
                    <div class="price">
                        <small class="starting">A partir de</small>
                        <strong class="current">R$<?php echo mysql_decimal_to_number($product->min_price); ?></strong>
                    </div>
                    <div class="payment">
                        <?php echo $this->load->view('_parcelas', array(
                            'productPrice' => $product->min_price,
                            'showMore' => FALSE
                        )); ?>
                    </div>
                    <?php if($product->promo){ ?>
                        <div class="offer">
                            <?php echo load_svg('arrow-down.svg'); ?>
                            <span class="save"><?php echo round($product->promo); ?>%<br/>off</span>
                        </div>
                    <?php } ?>
                <?php } if (($product->has_variation == '0' && $product->inventory >= 1) || $product->has_variation == '1'){ ?>
                    <div data-href="<?php echo site_url('produtos/'.$product->slug); ?>" class="common-button bt-buy"><span><?php echo load_svg('cart.svg'); ?> Comprar</span></div>
                <?php } ?>
            </a>
        </li>
    <?php } ?>
</ul>