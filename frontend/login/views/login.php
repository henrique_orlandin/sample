<div id="fb-root"></div>
<main id="login">

    <section class="common-header">
        <!-- título e breadcrumbs -->
        <div class="page-data">
            <div class="icon desktop-only"><?php echo load_svg('user.svg'); ?></div>
            <div class="text">
                <ul class="breadcrumbs">
                    <li class="breadcrumb-data"><a href="<?php echo site_url(); ?>">Página inicial</a></li>
                    <li class="breadcrumb-data"><strong>Entrar / Cadastre-se</strong></li>
                </ul>
                <div class="icon mobile-only"><?php echo load_svg('user.svg'); ?></div>
                <h1 class="title">Entre ou Cadastre-se</h1>
            </div>
        </div>

    </section>
    <div class="content-wrapper">

        <?php   if (isset($valid) && !$valid){ ?>
        <div class="invalid-hash">
            <i class="fa fa-exclamation"></i>O link que você está tentando acessar está expirado ou não existe.
        </div>
        <?php   } ?>
        <section id="login-wrapper">

            <button class="login-select mobile-only">
                <h3 class="title">Já sou cliente Hotmusic</h3>
                <p class="common-text">Clique aqui e cadastre-se agora!</p>
                <i class="fa fa-angle-right"></i>
            </button>

            <div class="login-content">

                <h2 class="title">Já sou cliente Hotmusic</h2>
                <div class="change-container">
                    <div id="login-box" class="login-container<?php echo $form == 'login' ? ' current' : ''; ?>" data-title="Login" data-slug="login">
                        <form action="<?php echo site_url('login/do-login'); ?>" method="POST" id="form-login" class="ajax-form common-form no-message" novalidate>
                            <div class="row">
                                <div class="form-group">
                                    <input type="email" name="email" id="login-email" placeholder="E-mail" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <input type="password" name="password" id="login-password" placeholder="Senha" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <a href="<?php echo site_url('login/esqueceu-sua-senha'); ?>" class="forgot-button nav-text">Esqueceu sua Senha?</a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <button type="submit" class="common-button">
                                        <span>Entrar</span>
                                        <?php $this->load->view('comum/preloader'); ?>
                                    </button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <p class="nav-text">Ou conecte com</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <button type="button" id="login-facebook" class="common-button login-facebook">
                                        <?php echo load_svg('facebook.svg'); ?>
                                        <span>Facebook</span>
                                        <?php $this->load->view('comum/preloader'); ?>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <?php   if (!(isset($hash) && isset($valid) && $valid)){ ?>
                    <div id="forgot-box" class="login-container<?php echo $form == 'forgot' ? ' current' : ''; ?>" data-title="Esqueceu sua senha?" data-slug="login/esqueceu-sua-senha">
                        <p class="common-text">Informe abaixo seu e-mail de cadastro e receba informações para redefinir sua senha.</p>
                        <form action="<?php echo site_url('login/forgot'); ?>" method="POST" id="form-forgot" class="ajax-form common-form" novalidate>
                            <div class="row">
                                <div class="form-group">
                                    <input type="email" name="email" id="forgot-email" placeholder="E-mail" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <button type="submit" class="common-button">
                                        <span>Enviar</span>
                                        <?php $this->load->view('comum/preloader'); ?>
                                    </button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <a href="<?php echo site_url('login'); ?>" class="back-button nav-text">Cancelar</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <?php   } else { ?>
                    <div id="change-box" class="login-container<?php echo $form == 'change' ? ' current' : ''; ?>" data-title="Alterar Senha" data-slug="login/esqueceu-sua-senha/<?php echo $hash; ?>">
                        <p class="common-text">Informe abaixo sua nova senha de acesso.</p>
                        <form action="<?php echo site_url('login/new-password'); ?>" method="POST" id="form-change" class="ajax-form common-form" novalidate>
                            <input type="hidden" name="hash" value="<?php echo $hash; ?>">
                            <div class="row">
                                <div class="form-group">
                                    <input type="password" name="password" id="change-password" placeholder="Senha" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <input type="password" name="password_confirm" id="change-confirm" placeholder="Repetir Senha" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <button type="submit" class="common-button">
                                        <span>Alterar</span>
                                        <?php $this->load->view('comum/preloader'); ?>
                                    </button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <a href="<?php echo site_url('login'); ?>" class="back-button nav-text">Cancelar</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <?php   } ?>
                </div>

            </div>

        </section>
        <section id="register-wrapper">

            <button class="login-select mobile-only">
                <h3 class="title">Quero ser cliente hotmusic</h3>
                <p class="common-text">Clique aqui para acessar sua conta.</p>
                <i class="fa fa-angle-right"></i>
            </button>

            <div class="login-content">

                <h2 class="title">Quero ser cliente Hotmusic</h2>
                <div class="face-register">
                    <p class="nav-text">Crie sua conta com</p>
                    <button type="button" id="register-facebook" class="common-button login-facebook">
                        <?php echo load_svg('facebook.svg'); ?>
                        <span>Facebook</span>
                    </button>
                </div>
                <form action="<?php echo site_url('login/register'); ?>" method="POST" id="form-register" class="ajax-form common-form" novalidate>
                    <p class="nav-text">ou preencha os campos</p>
                    <div class="wrapper-register">
                        <div class="change-form perfil current">
                            <div class="row">
                                <div class="form-group radio-group">
                                    <div class="common-radio">
                                        <input type="radio" name="type" id="radio-pf" value="pf" required checked />
                                        <div class="radio"></div>
                                        <label for="radio-pf">Pessoa Física</label>
                                    </div>
                                    <div class="common-radio">
                                        <input type="radio" name="type" id="radio-pj" value="pj" required />
                                        <div class="radio"></div>
                                        <label for="radio-pj">Pessoa jurídica</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <input type="text" name="name" placeholder="Nome" required>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="last_name" placeholder="Sobrenome" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <input type="email" name="email" placeholder="E-mail" required>
                                </div>
                                <div class="form-group">
                                    <input type="<?php echo $_is_mobile ? 'tel' : 'text'; ?>" name="cpf" id="input-doc" placeholder="CPF" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <input type="<?php echo $_is_mobile ? 'tel' : 'text'; ?>" name="phone" class="mask-phone" placeholder="Telefone" required>
                                </div>
                                <div class="form-group">
                                    <input type="<?php echo $_is_mobile ? 'tel' : 'text'; ?>" name="phone_mobile" class="mask-phone" placeholder="Celular">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="select-wrapper">
                                        <select name="gender" id="select-gender" required placeholder="SEXO">
                                            <option value="">SEXO</option>
                                            <option value="f">Feminino</option>
                                            <option value="m">Masculino</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="tel" name="birthdate" id="input-birthdate" class="mask-date" placeholder="Data de Nascimento" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <input type="password" name="password" id="new-password" placeholder="Senha" required>
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password_confirm" id="new-confirm" placeholder="Confirmar Senha" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group terms">
                                    <a class="common-button register-address"><span>Prosseguir</span></a>
                                </div>
                            </div>
                        </div>
                        <div class="change-form address-box">
                            <input type="hidden" name="address[label]" value="Casa">
                            <div class="row">
                                <div class="form-group">
                                    <?php $this->load->view('comum/preloader'); ?>
                                    <input type="<?php echo $_is_mobile ? 'tel' : 'text'; ?>" name="address[zipcode]" class="mask-zipcode" placeholder="CEP" required>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="address[street]" placeholder="Endereço" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <input type="number" name="address[number]" placeholder="Número" required>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="address[suburb]" placeholder="Bairro" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group relative">
                                    <?php $this->load->view('comum/preloader'); ?>
                                    <div class="select-wrapper">
                                        <select name="address[state]" id="address-state" required placeholder="ESTADO">
                                            <option value="">ESTADO</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group relative">
                                    <?php $this->load->view('comum/preloader'); ?>
                                    <div class="select-wrapper">
                                        <select name="address[city]" id="address-city" class="search-select" required placeholder="CIDADE">
                                            <option value="">CIDADE</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group terms">
                                    <div class="common-checkbox">
                                        <input type="checkbox" name="newsletter" id="check-news" value="1" />
                                        <div class="checkbox"></div>
                                        <label for="check-news">Desejo receber promoções e ofertas da Hotmusic</label>
                                    </div>
                                    <button class="register-back nav-text"><span>Voltar</span></button>
                                    <button type="submit" class="common-button">
                                        <?php $this->load->view('comum/preloader'); ?>
                                        <span>Criar Conta</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

            </div>

        </section>
    </div>
    <aside class="common-advantages">
        <?php $this->load->view('comum/advantages'); ?>
    </aside>
</main>