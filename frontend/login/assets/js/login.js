
function Login(){ this.init(); };

Login.prototype.init = function() {

    var address = new Address($('#form-register'));

    var self = this,
        animating = false;

    self.login_face();
    self.navigation();
    self.form_settings();

    if ($('.invalid-hash').length){
        setTimeout(function() {
            app.dialog.open({
                class: 'alert',
                title: 'Ops..',
                message: 'O link que você está tentando acessar está expirado ou não existe mais.'
            });
        }, 500);
    }

};

Login.prototype.login_face = function() {
    var user = new Object();
    $('.login-facebook').on('click', function(){
        var $form = $(this).closest('form');
        FB.login( function(response) {
            $form.addClass('sending');
            if (response.status === "connected") {
                var userID = response.authResponse.userID;
                FB.api("/me", {fields: "id, email, first_name, last_name, location, gender, picture"}, function (res) {
                    if (res && !res.error) {
                        user.facebook = res.id;
                        user.name = res.first_name;
                        user.last_name = res.last_name;
                        user.email = res.email;
                        user.gender = res.gender;
                        user.picture = res.picture.data.url;
                        $.ajax({
                            url: base_url + 'login/facebook-login',
                            dataType: 'json',
                            type: 'POST',
                            data: user,
                            success: function (data){
                                if (data.status){
                                    window.location = data.redirect;
                                }else{
                                    $form.removeClass('sending');
                                    app.dialog.open({
                                        'class': data.class ? data.class : 'alert',
                                        'title': data.title,
                                        'message': data.message
                                    });
                                }
                            },
                            error: function(){
                                $form.removeClass('sending');
                                app.dialog.open({
                                    'class': 'error',
                                    'title': 'Ocorreu um erro.',
                                    'message': 'Verifique seus dados e tente novamente.'
                                });
                            }
                        });
                    }else{
                        $form.removeClass('sending');
                        app.dialog.open({
                            class: 'error',
                            title: 'Ocorreu um erro',
                            message: 'Por favor, tente novamente mais tarde.'
                        });
                    }
                });
            }else{
                $form.removeClass('sending');
                app.dialog.open({
                    class: 'error',
                    title: 'Ocorreu um erro',
                    message: 'Por favor, tente novamente mais tarde.'
                });
            }
        }, { scope: 'email,public_profile' });
    });

};

Login.prototype.navigation = function() {

    var history = new History();

    $('.forgot-button').on('click', function(e){
        e.preventDefault();
        $('#login-box').removeClass('current');
        $('#login-box').next('.login-container').addClass('current');
        var $container = $('#login-box').next('.login-container');
        history.pushHistory({
            url: $container.data('slug'),
            title: 'Hotmusic - '+$container.data('title')
        });
    });

    $('.back-button').on('click', function(e){
        e.preventDefault();
        $('#login-box').next('.login-container').removeClass('current');
        $('#login-box').addClass('current');
        var $container = $('#login-box');
        history.pushHistory({
            url: $container.data('slug'),
            title: 'Hotmusic - '+$container.data('title')
        });
    });

    history.popHistory(function(url){
        var exploded = url.split('/');

        if (exploded.length > 1 && exploded[1] == 'esqueceu-sua-senha'){
            $('.forgot-button').click();
        }else{
            $('.back-button').click();
        }
    });
};

Login.prototype.form_settings = function() {

    //troca formulários no mobile
    $('.login-select').on('click',function() {
        $('#login .content-wrapper > section').removeClass('open');
        $(this).closest('section').addClass('open');
    });

    //validações para campos específicos
    if ($('#change-confirm').length){
        $('#change-confirm').rules( "add", {
            equalTo: '#change-password'
        });
    }

    if ($('#new-confirm').length){
        $('#new-confirm').rules( "add", {
            equalTo: '#new-password'
        });
    }

    $.validator.addMethod("cpf", function(value, element) {
      return this.optional(element) || validate_cpf(value);
    }, "O CPF informado não está correto!");

    $.validator.addMethod("cnpj", function(value, element) {
      return this.optional(element) || validate_cnpj(value);
    }, "O CNPJ informado não está correto!");

    $('#input-doc').mask('999.999.999-99').rules( "add", { cpf: true });

    //troca de pessoa física e jurídica
    $('input[name="type"]').on('change',function () {
        if($('input[name="type"]:checked').val() == 'pf') {
            $('#select-gender, #input-birthdate').attr('required','required').rules( "add", { required: true });
            $('#select-gender, #input-birthdate').closest('.row').removeClass('hide');
            $('#input-doc').attr('placeholder', 'CPF').val('').removeClass('error').mask('999.999.999-99').rules( "add", {cpf: true});
            $('#input-doc').rules( "remove", "cnpj");
        } else {
            $('#select-gender, #input-birthdate').removeAttr('required').rules( "remove", "required");
            $('#select-gender, #input-birthdate').closest('.row').addClass('hide');
            $('#input-doc').attr('placeholder', 'CNPJ').val('').removeClass('error').mask('99.999.999/9999-99').rules( "add", {cnpj: true});
            $('#input-doc').rules( "remove", "cpf" );
        }
    });

    //troca o form de cadastro para primeira ou segunda etapa
    $('.register-address').on('click', function(e){
        e.preventDefault();

        var valid = true;
        $('#form-register .perfil').find('input,select').each(function(i,el){
            if(!$(el).valid())
                valid = false;
        });

        if(valid) {
            $('#form-register .change-form').removeClass('current');
            $('#form-register .address-box').addClass('current');
        }

        if(is_mobile) {
            $('#wrapper').stop().animate({ scrollTop: 250 },800);
        }
    });

    $('.register-back').on('click', function(e){
        e.preventDefault();
        $('#form-register .change-form').removeClass('current');
        $('#form-register .perfil').addClass('current');

        if(is_mobile) {
            $('#wrapper').stop().animate({ scrollTop: 250 },800);
        }
    });
};

$(document).ready(function (){
    new Login();
});