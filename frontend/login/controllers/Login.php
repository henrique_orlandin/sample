<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH."libraries/activecampaign/ActiveCampaign.class.php");

class Login extends MY_Controller
{

    public function index($slug = FALSE, $hash = FALSE)
    {

        if ($this->st->client->get()){
            redirect('home');
        }

        if ($slug){
            $slug = str_replace('_', '-', $slug);
            switch ($slug) {
                case 'esqueceu-sua-senha':  $title = 'Esqueceu sua Senha?'; $form = 'forgot'; break;
                case 'criar-conta':  $title = 'Criar Conta'; $form = 'register'; break;
                default: show_404(); break;
            }
            if ($slug == 'esqueceu-sua-senha' && $hash){
                // Confere se hash é válida
                $valid = $this->st->client->check_hash($hash);
                $this->template->set('hash', $hash);
                $this->template->set('valid', $valid);
                $form = 'change';
                if (!$valid){
                    $title = 'Login';
                    $form = 'login';
                }
                else{
                    $title = 'Alterar Senha';
                }
            }else{

            }
        }else{
            $title = 'Login';
            $form = 'login';
        }

        $route = array('Login');
        $this->template->set('title', 'Hotmusic - '.$title)
                       ->add_css('css/login')
                       ->add_js('js/address', 'comum')
                       ->add_js('js/login')
                       ->set('route', $route)
                       ->set('form', $form)
                       ->build('login');
    }

    public function do_login()
    {
        if (!$this->input->is_ajax_request())
            show_404();

        // Validação
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'E-mail', 'trim|required');
        $this->form_validation->set_rules('password', 'Senha', 'trim|required');

        if ($this->form_validation->run()) {

            $data = $this->input->post();

            $this->st->client->login($data);

            $response = array(
                'status'=> TRUE,
                'redirect' => $this->session->userdata('st_autocheckout') ? site_url('carrinho') : ( !empty($_SESSION[Storetrooper::$session_id]['cart']) ? site_url('carrinho') : site_url('home'))
            );

        } else {
            $error = $this->form_validation->error_array();
            $response = array('status'=> FALSE, 'class' => 'error', 'title' => 'Ocorreu um erro!', 'message' => (count($error) > 1 ? 'Preencha os campos corretamente!' : array_shift($error)), 'fail' => $this->form_validation->error_array(TRUE));
        }

        $this->output->set_content_type('application/json')
                     ->set_output(json_encode($response));
    }

    public function facebook_login()
    {
        if (!$this->input->is_ajax_request())
            show_404();

        if ($client = $this->st->client->facebook_login($this->input->post())){

            $response = array(
                'status'=> TRUE,
                'message' => FALSE,
                'redirect' => $this->session->userdata('st_autocheckout') ? site_url('carrinho/carrinho') : ( !empty($_SESSION[Storetrooper::$session_id]['cart']) ? site_url('carrinho') : site_url('home'))
            );

        } else {
            $response = array('status'=> FALSE, 'class' => 'error', 'title' => 'Ocorreu um erro!', 'message' => 'Ocorre um problema inesperado. Por favor, tente novamente mais tarde.');
        }

        $this->output->set_content_type('application/json')
                     ->set_output(json_encode($response));
    }

    public function logout()
    {
        $this->st->client->logout();
        redirect('home');
    }

    public function forgot()
    {

        if (!$this->input->is_ajax_request())
            show_404();

        // Validação
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'E-mail', 'required|valid_email');

        if ($this->form_validation->run()) {

            $email = $this->input->post('email');

            $retrieve = $this->st->client->retrieve_password($email);

            if ($retrieve) {
                $response = array(
                    'status'=> TRUE,
                    'class' => 'success',
                    'title' => 'Sucesso!',
                    'message' => 'Enviamos um link ao seu e-mail para que possa alterar a sua senha.'
                );
            }else{
                $response = array(
                    'status'=> FALSE,
                    'class' => 'error',
                    'title' => 'Ocorreu um erro!',
                    'message' => 'E-mail não cadastrado no sistema.'
                );
            }
        }else{
            $error = $this->form_validation->error_array();
            $response = array(
                'status'=> FALSE,
                'class' => 'error',
                'title' => 'Ocorreu um erro!',
                'message' => (count($error) > 1 ? 'Preencha os campos corretamente!' : array_shift($error)),
                'fail' => $this->form_validation->error_array(TRUE)
            );
        }

        $this->output->set_content_type('application/json')
                     ->set_output(json_encode($response));
    }

    public function new_password()
    {
        if (!$this->input->is_ajax_request())
            show_404();

        // Validação
        $this->load->library('form_validation');

        $this->form_validation->set_rules('password', 'Nova Senha', 'required|matches[password_confirm]|trim|min_length[8]');
        $this->form_validation->set_rules('password_confirm', 'Repetir Senha', 'required|trim|min_length[8]');
        $this->form_validation->set_rules('hash', 'Hash', 'required');

        // Mensagens
        $this->form_validation->set_message('matches', 'Os campos de senhas são obrigatórios e devem ser preenchidos com a mesma senha.');
        $this->form_validation->set_message('min_length', 'Sua senha deve conter ao menos 8 caracteres.');

        if ($this->form_validation->run()) {

            $client = $this->st->client->new_password($this->input->post());

            if ($client) {

                $response = array(
                    'status'=> TRUE,
                    'class' => 'success',
                    'title' => 'Sucesso!',
                    'message' => 'Sua senha foi redefinida com sucesso!',
                    'redirect' => $this->session->userdata('st_autocheckout') ? site_url('carrinho/checkout') : ( !empty($_SESSION[Storetrooper::$session_id]['cart']) ? site_url('carrinho') : site_url('home'))
                );
            }else{
                $response = array('status'=> FALSE,
                    'class' => 'error',
                    'title' => 'Ocorreu um erro!',
                    'message' => 'Ocorreu um erro na atualização da senha.'
                );
            }
        }else{
            $error = $this->form_validation->error_array();
            $response = array(
                'status'=> FALSE,
                'class' => 'error',
                'title' => 'Ocorreu um erro!',
                'message' => (count($error) > 1 ? 'Preencha os campos corretamente!' : array_shift($error)),
                'fail' => $this->form_validation->error_array(TRUE)
            );
        }

        $this->output->set_content_type('application/json')
                     ->set_output(json_encode($response));
    }

    /**
     * AJAX Cadastro
     */
    public function register()
    {
        if (!$this->input->is_ajax_request())
            show_404();

        // Validação
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Nome', 'trim|required');
        $this->form_validation->set_rules('last_name', 'Sobrenome', 'trim|required');
        $this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email');
        $this->form_validation->set_rules('phone', 'Telefone', 'trim|required');
        if($this->input->post('type') == 'pf') {
            $this->form_validation->set_rules('gender', 'Sexo', 'trim|required');
            $this->form_validation->set_rules('birthdate', 'Data de Nascimento', 'trim|required');
            $this->form_validation->set_rules('cpf', 'CPF', 'trim|required|valid_cpf');
        } else
            $this->form_validation->set_rules('cpf', 'CNPJ', 'trim|required|valid_cnpj');
        $this->form_validation->set_rules('password', 'Senha', 'trim|required|matches[password_confirm]|trim|min_length[8]');
        $this->form_validation->set_rules('password_confirm', 'Repetir Senha', 'trim|required|trim|min_length[8]');


        // Endereço
        $this->form_validation->set_rules('address[zipcode]', 'CEP', 'trim|required');
        $this->form_validation->set_rules('address[street]', 'Endereço', 'trim|required');
        $this->form_validation->set_rules('address[number]', 'Número', 'trim|required');
        $this->form_validation->set_rules('address[suburb]', 'Bairro', 'trim|required');
        $this->form_validation->set_rules('address[state]', 'Estado', 'trim|required');
        $this->form_validation->set_rules('address[city]', 'Cidade', 'trim|required');

        // Mensagens
        $this->form_validation->set_message('matches', 'Os campos de senhas são obrigatórios e devem ser preenchidos com a mesma senha.');
        $this->form_validation->set_message('min_length', 'Sua senha deve conter ao menos 8 caracteres.');

        if ($this->form_validation->run()) {

            $data = $this->input->post();
            if($data['type'] == 'pj') {
                $data['gender'] = 'j';
            }
            $client = $this->st->client->add($data);

            // Active Campaign
            $ac = new ActiveCampaign(ACTIVECAMPAIGN_URL, ACTIVECAMPAIGN_API_KEY);

            if ((int)$ac->credentials_test()) {

                $list_id = ACTIVECAMPAIGN_LIST_ID;

                $contact = array(
                    "email"              => $data['email'],
                    "first_name"         => $data['name'],
                    "last_name"          => $data['last_name'],
                    "p[{$list_id}]"      => $list_id,
                    "status[{$list_id}]" => 1,
                );
                $contact_sync = $ac->api("contact/sync", $contact);
            }
            // Fim Active Campaign

            $response = array(
                'status'=> TRUE,
                'class' => 'success',
                'title' => 'Sucesso!',
                'message' => 'Cadastro efetuado com sucesso!',
                'track_push' => array(
                    'fb' => array(
                        'action' => 'CompleteRegistration',
                        'body' => array()
                    )
                ),
                'redirect' => $this->session->userdata('st_autocheckout') ? site_url('carrinho/carrinho') : ( !empty($_SESSION[Storetrooper::$session_id]['cart']) ? site_url('carrinho') : site_url('home'))
            );
        } else {
            $error = $this->form_validation->error_array();
            $response = array(
                'status' => FALSE,
                'class' => 'error',
                'title' => 'Ocorreu um erro!',
                'message' => (count($error) > 1 ? 'Preencha os campos corretamente!' : array_shift($error)),
                'fail' => $this->form_validation->error_array(TRUE)
            );
        }

        $this->output->set_content_type('application/json')
                     ->set_output(json_encode($response));
    }

    public function _remap($method, $params = array())
    {
        if (method_exists($this, $method)){
            return call_user_func_array(array($this, $method), $params);
        }else{
            array_unshift($params, $method);
            return call_user_func_array(array($this, 'index'), $params);
        }
    }

}
