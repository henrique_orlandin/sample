
"use strict";

var carrinho;

function Carrinho(){ return this; };

Carrinho.prototype.init = function() {
    var self = this;

    this.initQuantityFields();
    this.navigation();
    this.calc_height();
    this.config_address();
    this.change_address();
    this.change_local_shipping();

    $(window).on('resize', function() {
        if($('#my-cart .address-wrapper').height() != $('#my-cart .change-content:not(.hide)').height()) {
            self.calc_height();
        }
    });

    // Remover
    $('#my-cart').on('click', '.button-remove', function(e){
        e.preventDefault();
        var $content = $(this).closest('.order-products-data'),
            $button = $(this);
        app.dialog.open({
            class: 'alert',
            cancel: true,
            button: 'Remover',
            title: 'Deseja remover o produto?',
            message: 'Você tem certeza que deseja remover o produto de seu carrinho?',
            callback: function(){
                $button.hide();
                $.post(base_url+'carrinho/remove',{id: $button.data('id')}, function(data){
                    $button.show();
                    $content.slideUp(function(){
                        $(this).remove();
                        $('#cart-quantity').text(data.total);
                        $('.common-header .cart-items').html(data.total + ' produto' + (data.total != 1 ? 's' : ''));
                        if(data.total == 0) {
                            $('#cart-quantity').closest('.active').removeClass('active');
                            $('.common-header .cart-items').fadeOut();
                        }
                        if ($('.order-products-data').length == 0){
                            $('#form-cart').after('<div class="common-empty"><p>Nenhum produto no seu carrinho.</p><a class="common-button" href="' + base_url + 'produtos"><span>Voltar às compras</span></a></div>');
                            $('#form-cart').remove();
                        } else {
                            carrinho.cart_update();
                        }
                    });
                });
            }
        });
    });

    if ($('#form-cart').length)
        self.calculate();

    $('#coupon_input').on('keydown', function(e){
        if(e.keyCode == 13) {
            e.preventDefault();
            $('#apply-cupom').click();
            return false;
        }
    });
    $('#apply-cupom').on('click', function(){
        if ($(this).hasClass('loading'))
            return;
        var v = $('#coupon_input').val().trim();
        if (v.length > 5){
            $(this).addClass('loading');
            var xhr = $(this).data('xhr');
            if (xhr !== undefined){
                xhr.abort();
            }
            $(this).data('xhr', $.post(base_url+'carrinho/check-coupon',{coupon: v}, function(data){
                if (data.status){
                    $('.price-coupon').data('value',data.value);
                    $('.price-coupon').data('type',data.type);
                    $('#apply-cupom').off('click');
                    $('#cart-coupon').fadeOut(400, function(){
                        $('#coupom-applied').fadeIn(400);
                    });
                    self.calculate();
                }
                app.dialog.open({
                    class: data.class,
                    title: data.title,
                    message: data.message,
                    callback: function(){
                        $('#coupon').focus();
                    }
                });
                $(this).removeData('xhr').removeClass('loading');
            }.bind(this)));
        }else{
            app.dialog.open({
                class: 'error',
                title: 'Cupom Inválido',
                message: 'Digite um código de cupom válido.',
                    callback: function(){
                        $('#coupon').focus();
                    }
            });
        }
    });

    $('button.success').on('click',function(){
        $('#form-cart').submit();
    });

    if($('#address-container .address-content').length == 1) {
        $('#address-container .address-content:first input').trigger('click');
    }

    $('body').on('change', 'input[name="shipping"]', function (){
        $('#form-cart').find('input[name="id_shipping"]').val($(this).val());
        self.calculate();
    });

};

Carrinho.prototype.calculate = function(){

    var self = this,
        subtotal = 0,
        total = 0,
        coupon = $('.price-coupon').data('type') == 'percent' ? $('.price-coupon').data('value') : $('.price-coupon').data('value'),
        freight_val = $('input[name="shipping"]:checked').length ? app.getMoney($('input[name="shipping"]:checked').data('price')) : 0;

    $('#form-cart').find('.total-price').each(function(){
        subtotal += app.getMoney($(this).html());
    });
    $('.price-subtotal .data').html(app.formatReal(subtotal));
    $('.price-freight .data').html(app.formatReal(freight_val));
    if($('.price-coupon').data('type') == 'percent') {
        coupon *= subtotal;
    }
    $('.price-coupon .data').html('-'+app.formatReal(coupon));
    total = subtotal - coupon + freight_val;

    if (total < 0)
        total = 0;

    $.post(base_url + 'carrinho/get-total-payment',{ total: total },function (data) {
        if(data.status) {
            $('.price-total').prop('data-total', total);
            $('.price-total .data').html(data.view);
        }
    });
};

Carrinho.prototype.initQuantityFields = function(){
    var $quantity = $('.quantity-field');

    // Number
    $quantity.on('keyup', function(e){
        var $parent = $(this).closest('.price-info'),
            $unit_price = $parent.find('.unit-price'),
            quantity = parseInt($(this).val());


        if (quantity){
            var unit = app.getMoney($unit_price.html());
            $parent.find('.total-price').html(app.formatReal(unit * quantity));
            carrinho.cart_update();
        }
    });
};

Carrinho.prototype.navigation = function(){
    var self = this;
    $('body').on('click','#add-address',function(event) {
        self.load_form();
        self.calc_height();
    });
    $('body').on('click','#new-address .common-back',function(event) {
        $('#new-address').addClass('hide');
        $('#address-container').removeClass('hide');
        self.calc_height();
    });
};

Carrinho.prototype.calc_height = function(){
    setTimeout(function(){
        $('#my-cart .address-wrapper').stop().animate({ height: $('#my-cart .change-content:not(.hide)').height() },500);
    },300);
};

Carrinho.prototype.load_form = function(id){
    $('#new-address').removeClass('hide');
    $('#address-container').addClass('hide');
};

Carrinho.prototype.config_address = function(){

    app.setForm($('#form-address'), function(data){
        if (data.status){
            var $view = $(data.view),
                $box = $('#address-container .address-content[data-id="'+data.address.id+'"]');

            $view.appendTo('#address-container .address-list');
            $('#new-address .common-back').trigger('click');
            $('#address-container .address-content:last input').trigger('click');
        }
    });
    new Address($('#form-address'));
};

Carrinho.prototype.change_address = function(){
    var self = this;
    $('body').on('change', 'input[name="address"]', function (){
        $('#form-cart').find('input[name="id_address"]').val($(this).val());
        $('#form-cart').find('input[name="id_shipping"]').val('');
        $('#form-cart').find('input[name="pickup_at_store"]').val('');
        if($('.shipping-option').css('display') == 'none') {
            $('.shipping-option').fadeIn();
        }
        if(parseInt($(this).val()) > 0) {
            $(this).closest('.address-content').addClass('loading');
            $('#my-cart').find('#check-shipping').prop('checked', false);
            app.calculateShipping({
                zipcode: $('input[name="address"]:checked').data('zipcode'),
                product: null,
                view: 'carrinho/shipping',
                bt: $('#cart-prices button[type="submit"]'),
                element: $(this)
            });
        } else {
            $('.shipping-option').fadeOut(function(){ carrinho.calc_height(); });
        }
    });
    $('input[name="address"]:checked').change();
};

Carrinho.prototype.change_local_shipping = function(){
    var self = this;
    $('body').on('change','#check-shipping',function(){
        if($('#check-shipping:checked').length) {
            $('#form-cart').find('input[name="id_address"]').val('');
            $('#form-cart').find('input[name="id_shipping"]').val('');
            $('#form-cart').find('input[name="pickup_at_store"]').val('1');
            $('.shipping-option').fadeOut(function(){ carrinho.calculate(); });
            $('#my-cart').find('input[name="address"],input[name="shipping"]').prop('checked', false);
            $('#address-container .address-list').slideUp(function(){ $('#local-shipping-message').slideDown(function() {
                carrinho.calc_height();
            }); });
            $('#add-address').fadeOut();
        } else {
            $('#local-shipping-message').slideUp(function(){ $('#address-container .address-list').slideDown(function() {
                carrinho.calc_height();
            }); });
            $('#add-address').fadeIn();
        }
    });
}

Carrinho.prototype.cart_update = function() {
    $.post(base_url+'carrinho/save-cart',$('#form-cart').serialize(), function(data){
        if($('input[name="address"]:checked').length) {
            $('input[name="address"]:checked').trigger('change');
        } else {
            carrinho.calculate();
        }
    });
};

$(document).ready(function (){
    carrinho = new Carrinho();
    carrinho.init();
});