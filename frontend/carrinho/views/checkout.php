<main id="checkout">

    <section class="common-header">

        <div class="page-data">
            <div class="icon desktop-only"><?php echo load_svg('credit-card.svg'); ?></div>
            <div class="text">
                <ul class="breadcrumbs">
                    <li class="breadcrumb-data"><a href="<?php echo site_url(); ?>">Página Incial</a></li>
                    <li class="breadcrumb-data"><strong>Carrinho</strong></li>
                </ul>
                <div class="icon mobile-only"><?php echo load_svg('credit-card.svg'); ?></div>
                <h1 class="title">Pagamento</h1>
            </div>
        </div>

    </section>

    <section class="content">
        <form action="<?php echo site_url('carrinho/checkout/finish'); ?>" method="POST" id="form-checkout" class="common-form ajax-form no-message" novalidate>
            <div class="payment-method">
                <div class="main-title">Formas de Pagamento</div>
                <ul class="payment-type-list">
                    <li class="common-radio">
                        <input type="radio" name="method" id="radio-credit-card" value="credit_card" required checked />
                        <div class="radio"></div>
                        <label for="radio-credit-card">Cartão de Crédito</label>
                    </li>
                    <li class="common-radio">
                        <input type="radio" name="method" id="radio-transfer" value="bank_transfer" required />
                        <div class="radio"></div>
                        <label for="radio-transfer">Transferência bancária <?php echo DEBT_PRICE_DISCOUNT ? '(-' . DEBT_PRICE_DISCOUNT . '%)' : ''; ?></label>
                    </li>
                    <?php /*<li class="common-radio">
                        <input type="radio" name="method" id="radio-slip" value="bankslip" required />
                        <div class="radio"></div>
                        <label for="radio-slip">Boleto</label>
                    </li>*/ ?>
                </ul>
                <div id="credit_card" class="payment-tab">
                    <div class='card-wrapper'></div>

                    <input type="hidden" id="checkout-card" name="card[brand]">

                    <div class="row">
                        <div class="form-group">
                            <label for="checkout-number">Número do Cartão</label>
                            <input type="<?php echo $_is_mobile ? 'tel' : 'text'; ?>" name="card[number]" id="checkout-number" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="checkout-name">Nome impresso no Cartão</label>
                            <input type="text" name="card[name]" id="checkout-name" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="checkout-name">CPF do titular do cartão</label>
                            <input type="text" name="card[cpf]" value="<?php echo $full_client->cpf; ?>" class="mask-cpf" id="checkout-cpf" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group expiration">
                            <label for="checkout-expiration">Data de Validade</label>
                            <input type="number" name="card[expiration_month]" id="checkout-expiration-month" min="1" max="12" minlength="2" maxlength="2" required>
                            <span>/</span>
                            <input type="number" name="card[expiration_year]" id="checkout-expiration-year" min="<?php echo date('y'); ?>" max="<?php echo (date('y') + 15); ?>" maxlength="2" minlength="2" required>
                        </div>
                        <div class="form-group security">
                            <label for="checkout-cvc">Código de Segurança</label>
                            <input type="number" name="card[cvc]" id="checkout-cvc" required maxlength="3">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="checkout-payment">Opções de parcelamento</label>
                            <div class="select-wrapper">
                                <select name="installments" id="checkout-payment" class="select2">
                                    <option value="1" selected="selected">1x de R$ <?php echo mysql_decimal_to_number( $cart_amount ); ?></option>
                                    <?php for ($i = 2; $i <= MAX_INSTALLMENTS; $i++){ ?>
                                        <?php if ($cart_amount / $i > MIN_INSTALLMENT_PRICE){ ?>
                                            <option value="<?php echo $i; ?>"><?php echo $i; ?>x de R$ <?php echo mysql_decimal_to_number( bcdiv($cart_amount, $i, 2) ); ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="bank_transfer" class="payment-tab">
                    <p>Conclua o seu pedido no botão <strong>Finalizar Compra</strong>. Após isso, faça uma transferência bancária e envie-nos um e-mail com o comprovante!</p>
                    <div class="bank-info"><?php echo $debt_account->description; ?></div>
                </div>
            </div>
            <div class="order-summary">
                <div id="shipping-container">
                    <div class="main-title">Endereço de entrega:</div>
                    <?php if ($resume->shipping !== '' && $resume->shipping !== null && $resume->shipping !== FALSE){ ?>
                        <div class="address-data">
                            <?php echo load_svg('marker.svg'); ?>
                            <div class="text">
                                <h4 class="title"><?php echo $address->label; ?></h4>
                                <div class="common-text">
                                    <p>
                                        <?php echo $address->street.', '.$address->number.($address->complement ? ', '.$address->complement : '').
                                        ', Bairro: '.$address->suburb.' - '.$address->zipcode.', '.$address->city.' - '.$address->uf; ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="shipping-data">
                            <p>
                                <?php
                                    foreach ($shipping as $key => $val) {
                                        if($resume->shipping == $val->id){ ?>
                                        <strong<?php echo $val->price > 0 ? '' : ' class="free"'; ?>><?php echo (int) $val->price > 0 ? $val->title : 'FRETE GRÁTIS'; ?>
                                        Sedex
                                        </strong>
                                         <?php
                                            echo $val->days.($val->days == 1 ? ' dia útil' : ' dias úteis');
                                        }
                                    }
                                ?>
                            </p>
                        </div>
                    <?php } else { ?>
                        <div class="address-data">
                            <?php echo load_svg('marker.svg'); ?>
                            <div class="text">
                                <h4 class="title">Retirada na Loja</h4>
                                <div class="common-text">
                                    <p>
                                        Rua Coronel Flores, 376, Bairro: São Pelegrino 95034-060, Caxias do Sul - RS
                                    </p>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>

                <div class="main-title">Resumo do Pedido</div>
                <div class="products">
                    <?php foreach ($cart as $key => $item) { ?>
                        <div class="product">
                            <?php
                            $options = array(
                                'src'           => resize_url('image/resize?w=140&h=140&src='.$item->image),
                                'alt'           => $item->title . ($item->variation ? '(' . $item->variation->title . ')' : ''),
                                'class'         => 'lazyload',
                                'data-viewport' => 1,
                                'tag'           => 'a',
                                'href'          => site_url('produtos/'.$item->slug)
                            );
                            echo lazyload($options);
                            ?>
                            <div class="info">
                                <a href="<?php echo site_url('produtos/'.$item->slug); ?>">
                                    <h3 class="title"><?php echo $item->title; ?>
                                        <?php if ($item->variation){ ?>
                                        <small class="optional">(<?php echo $item->variation->title; ?>)</small>
                                        <?php } ?>
                                    </h3>
                                    <div class="quantity">Quantidade <?php echo $item->quantity . ((int) $item->quantity > 1 ? ' x R$ ' . mysql_decimal_to_number($item->price) : ''); ?></div>
                                    <div class="price">R$<?php echo mysql_decimal_to_number((float) $item->price * (int) $item->quantity); ?></div>
                                </a>
                            </div>
                        </div>
                    <?php   }  ?>
                </div>

                <?php $this->load->view('resumo', array('isCheckoutResume' => TRUE)); ?>

                <div class="navigation">
                    <button type="submit" class="common-button success">
                        <?php $this->load->view('comum/preloader'); ?>
                        <?php echo load_svg('correct.svg'); ?>
                        <span>Finalizar Compra</span>
                    </button>
                </div>

            </div>
        </form>
    </section>
    <aside class="common-advantages">
        <?php $this->load->view('comum/advantages'); ?>
    </aside>
</main>
<!-- Google Code for Compras Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 848834015;
var google_conversion_label = "9x9CCPyTpocBEN_b4JQD";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript"
src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt=""
src="//www.googleadservices.com/pagead/conversion/848834015/?label=9x9CCPyTpocBEN_b4JQD&amp;guid=ON&amp;script=0"/>
</div>
</noscript>