<main id="checkout">

    <section class="common-header">

        <div class="page-data">
            <div class="icon desktop-only"><?php echo load_svg('credit-card.svg'); ?></div>
            <div class="text">
                <ul class="breadcrumbs">
                    <li class="breadcrumb-data"><a href="<?php echo site_url(); ?>">Página Incial</a></li>
                    <li class="breadcrumb-data"><strong>Carrinho</strong></li>
                </ul>
                <div class="icon mobile-only"><?php echo load_svg('credit-card.svg'); ?></div>
                <h1 class="title">Pagamento</h1>
            </div>
        </div>

    </section>
    <section id="checkout-success">
        <div class="common-limiter">
            <?php if (!$order->id_status){ ?>
                <div class="success-title warning-title">Pedido recebido: Aguardando pagamento!</div>
            <?php } else { ?>
                <div class="success-title">Pedido Realizado com Sucesso!</div>
            <?php } ?>
            <?php if ($order->method != 'bank_transfer'){ ?>
                <div class="success-subtitle">Seu pedido está em andamento</div>
            <?php } ?>
            <div class="success-number">Número do Pedido: <?php echo $reference; ?></div>
            <?php if ($order->method == 'bank_transfer'){ ?>
                <div class="success-text"><p>Seu pedido ainda não foi concluído. Realize a transferência bancária utilizando os dados abaixo.<br />Siga as seguintes instruções:</p></div>
            <?php } else { ?>
                <div class="success-text"><p>Seu pedido foi realizado com sucesso. Acompanhe pela &aacute;rea dos seus pedidos.</p></div>
            <?php } ?>
            <?php if ($order->method == 'bank_transfer'){ ?>
                <div class="success-text bank-transfer-text"><strong>Realize sua transferência:</strong><br /><br /><?php echo $debt_account->description; ?></div>
            <?php } ?>
            <div class="success-button">
                <a href="<?php echo site_url('minha-conta/meus-pedidos/'.$reference); ?>" class="common-button"><span>Acompanhar Pedido</span></a>
            </div>
        </div>
    </section>

    <aside class="common-advantages">
        <?php $this->load->view('comum/advantages'); ?>
    </aside>
</main>
<script type="text/javascript">
    fbq('track', 'Purchase', {'value':'<?php echo $order->price; ?>','currency':'BRL'});

    ga('ec:setAction', 'purchase', {
        'id': '<?php echo $reference; ?>',
        'affiliation': 'Hotmusic',
        'revenue': '<?php echo $order->price; ?>',
        'shipping': '<?php echo $order->freight; ?>'
    });
    ga('ec:addProduct', {
        'id': data.product.id,
        'name': data.product.title,
        'price': parseFloat(data.product.price),
        'quantity': data.quantity
    });
    ga("send", "event", "Successful Order", "click", "successOrder");
</script>