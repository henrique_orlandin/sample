<main id="my-cart">

    <section class="common-header">

        <div class="page-data">
            <div class="icon desktop-only"><?php echo load_svg('cart.svg'); ?></div>
            <div class="text">
                <ul class="breadcrumbs">
                    <li class="breadcrumb-data"><a href="<?php echo site_url('home'); ?>">Página Incial</a></li>
                    <li class="breadcrumb-data"><strong>Carrinho de Compras</strong></li>
                </ul>
                <div class="icon mobile-only"><?php echo load_svg('cart.svg'); ?></div>
                <h1 class="title">Carrinho de compras</h1>
                <?php if($total_cart > 0) { ?>
                <div class="cart-items"><?php echo $total_cart.' produto' . ($total_cart != 1 ? 's' : ''); ?></div>
                <?php } ?>
            </div>
        </div>

    </section>

    <section class="content">

        <?php if ( isset($cart) && !empty($cart) ){ ?>
        <form action="<?php echo site_url('carrinho/save-cart'); ?>" method="POST" id="form-cart" class="common-form ajax-form no-message no-reset" novalidate>

            <input type="hidden" name="id_address">
            <input type="hidden" name="id_shipping">
            <input type="hidden" name="pickup_at_store">

            <ul class="order-products-list" id="cart-products">
                <?php foreach ($cart as $key => $item) { ?>

                    <li class="order-products-data" data-weight="<?php echo $item->weight; ?>" data-variation="<?php echo $item->variation ? $item->variation->id : ''; ?>">
                        <?php
                        $options = array(
                            'src'           => resize_url('image/resize?w=140&h=140&src='.$item->image),
                            'alt'           => $item->title . ($item->variation ? '(' . $item->variation->title . ')' : ''),
                            'class'         => 'lazyload',
                            'data-viewport' => 1,
                            'tag'           => 'a',
                            'href'          => site_url('produtos/'.$item->slug)
                        );
                        echo lazyload($options);
                        ?>
                        <div class="info price-info">
                            <a href="<?php echo site_url('produtos/'.$item->slug); ?>">
                                <h2 class="title">
                                    <?php echo $item->title; ?>
                                    <small class="optional"><?php echo $item->variation ? '(' . $item->variation->title.' '.(isset($item->variation->value) ? $item->variation->value : '') . ')' : ''; ?></small>
                                </h2>
                            </a>
                            <div class="price">
                                <?php /* if($item->original_price != $item->price){ ?>
                                    <small class="old">De R$ <?php echo mysql_decimal_to_number($item->original_price); ?></small>
                                <?php } */ ?>
                                <strong class="current unit-price">Por R$ <?php echo mysql_decimal_to_number($item->price); ?></strong>
                            </div>
                            <div class="right">
                                <div class="quantity">
                                    <span class="label">Quant.</span>
                                    <input type="number" name="quantity[<?php echo $key ?>]" value="<?php echo $item->quantity; ?>" class="quantity-field mask-quantity">
                                </div>
                                <div class="total">
                                    <span class="label">Subtotal</span>
                                    <strong class="data total-price">R$<?php echo mysql_decimal_to_number((float) $item->price * (int) $item->quantity); ?></strong>
                                </div>
                            </div>
                        </div>
                        <button type="button" class="button-remove" data-id="<?php echo $key; ?>"><?php echo load_svg('trash.svg'); ?></button>
                    </li>
                <?php } ?>
            </ul>

        </form>

        <div id="cart-inputs">

            <?php if($client) { ?>
                <div class="common-checkbox local-shipping">
                    <label for="check-shipping">
                        <input type="checkbox" name="pickup_at_store" id="check-shipping" value="pickup_at_store" data-price="0" />
                        <div class="checkbox"></div>
                        Desejo retirar pessoalmente na loja física da hotmusic caxias do sul.
                    </label>
                </div>
            <?php } ?>

                <div class="common-text obs">
                    <?php echo $info_store->description; ?>
                </div>

            <?php if($client) { ?>
            <div class="address-wrapper">
                <div id="address-container" class="change-content">
                    <div class="content-top">
                        <h3 class="account-title">Endereço de Entrega</h3>
                        <button href="<?php echo site_url('minha-conta/meus-enderecos/endereco'); ?>" class="common-button colorbox" id="add-address">
                            <?php echo load_svg('more.svg'); ?>
                            <span>Adicionar endereço</span>
                        </button>
                    </div>
                    <div id="local-shipping-message">Você escolheu <strong>Retirada na Loja</strong></div>
                    <div class="address-list">
                        <?php $this->load->view('endereco'); ?>
                    </div>
                    <div class="shipping-option">
                        <h3 class="shipping-title">Selecione a forma de Entrega</h3>
                        <div id="shipping-list" class="shipping-result"></div>
                    </div>
                </div>

                <div id="new-address" class="change-content hide">
                    <?php $this->load->view('endereco_form'); ?>
                </div>
                <div class="content-loader">
                    <?php $this->load->view('comum/preloader'); ?>
                </div>
            </div>
            <?php } ?>

            <div class="cart-inputs common-form">

                <?php if(!$client) { ?>
                <div class="cart-inputs-dup">
                    <h4 class="title">Calcular Frete e Prazo</h4>
                    <div class="form-group" id="cart-zipcode">
                        <input type="<?php echo $_is_mobile ? 'tel' : 'text'; ?>" name="zipcode" id="zipcode" placeholder="CEP">
                        <button type="button" class="common-button" id="calculate" data-cart="1">
                            <?php $this->load->view('comum/preloader'); ?>
                            <span>Calcular</span>
                        </button>
                    </div>
                    <div class="shipping-result"></div>
                </div>
                <?php } ?>
                <div class="cart-inputs-dup">
                    <h4 class="title">Cupom de Desconto</h4>
                    <div class="form-group <?php echo ($coupon) ? 'hide' : ''; ?>" id="cart-coupon">
                        <input type="text" name="coupon" id="coupon_input" placeholder="Código" <?php echo ($coupon) ? 'value="'.$coupon->reference.'"' : ''; ?>>
                        <button type="button" class="common-button" id="apply-cupom">
                            <?php $this->load->view('comum/preloader'); ?>
                            <span>Incluir</span>
                        </button>
                    </div>
                    <div id="coupom-applied" <?php echo ($coupon) ? 'class="show"' : ''; ?>>Desconto do cupom aplicado com sucesso.</div>
                </div>

            </div>

            <div class="total">
                <?php $this->load->view('resumo'); ?>
            </div>

        </div>

        <div class="cart-navigation">
            <a href="<?php echo site_url('produtos') ?>" class="common-button">
                <?php echo load_svg('cart.svg'); ?>
                <span>Continuar Comprando</span>
            </a>
            <button type="submit" class="common-button success">
                <?php $this->load->view('comum/preloader'); ?>
                <?php echo load_svg('correct.svg'); ?>
                <span>Finalizar Compra</span>
            </button>
        </div>

        <?php
                }else{
        ?>
        <div class="common-empty">
            <p>Nenhum produto no seu carrinho.</p>
            <a class="common-button" href="<?php echo site_url('produtos'); ?>"><span>Voltar às compras</span></a>
        </div>
        <?php   } ?>

    </section>

    <aside class="common-advantages">
        <?php $this->load->view('comum/advantages'); ?>
    </aside>

</main>