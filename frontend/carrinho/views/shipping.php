<?php foreach ($options as $key => $option){ ?>
<div class="common-radio shipping-info">
    <input id="shipping-<?php echo $option->id; ?>" name="shipping" type="radio" value="<?php echo $option->id; ?>" data-price="<?php echo $option->price; ?>">
    <span class="radio"></span>
    <label class="label" for="shipping-<?php echo $option->id; ?>">

        <strong<?php echo $option->price > 0 ? '' : ' class="free"'; ?>><?php echo ($option->title.' - ') . ((int) $option->price > 0 ? '' : 'FRETE GRÁTIS - '); ?>
        </strong>
        <?php if ((int) $option->price > 0){ ?>
            Entrega em até <?php echo $option->days.($option->days == 1 ? ' dia útil' : ' dias úteis'); ?> - <span class="price"><?php echo (int) $option->price > 0 ? 'R$ '.mysql_decimal_to_number($option->price) : ''; ?></span>
        <?php } ?>

    </label>
</div>
<?php }
if(empty($options)) { ?>
<div class="no-shipping">Nenhuma forma de entrega encontrada. <a href="<?php echo site_url('contato'); ?>">Entre em contato conosco.</a></div>
<?php }  ?>