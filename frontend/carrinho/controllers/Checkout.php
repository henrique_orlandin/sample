<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends MY_Controller
{

    public function index()
    {

        if (empty($this->client) || !$this->client){
            $this->session->set_userdata('st_autocheckout', TRUE);
            redirect('login');
        }

        if (!$this->client->is_valid){
            $this->session->set_userdata('st_autocheckout', TRUE);
            redirect('minha-conta/meus-dados');
        }

        $cart = $this->st->cart->get();

        if (empty($cart)){
            redirect('carrinho');
        }

        $hasPromo = FALSE;
        foreach ($cart as $key => $each_product){
            if ($each_product->promo)
                $hasPromo = TRUE;
        }

        // Remove auto redirect
        $this->session->set_userdata('st_autocheckout', FALSE);
        $resume = $this->st->cart->get_resume();
        $address = $this->st->addresses->get(array('client' => $this->client->id, 'id' => $resume->address));
        $shipping = array();
        $shipping_value = 0;
        if(isset($resume->shipping) && ($resume->shipping !== '' && $resume->shipping !== null && $resume->shipping !== FALSE)){
            $shipping = $this->st->shipping->get(array(
                'zipcode' => $address->zipcode,
                'price' => $resume->total
            ));
            foreach ($shipping as $freight) {
                if ($freight->id == $resume->shipping) {
                    $shipping_value = $freight->price;
                }
            }
        }
        $coupon_discount = 0;
        if (isset($resume->coupon)) {
            $coupon_value = !$resume->coupon ? 0 : (float)$resume->coupon->value/100;
            $coupon_discount = $coupon_value * $resume->subtotal;
        }

        $cart_amount = $resume->total + $shipping_value - $coupon_discount;

        $this->template
             ->add_css('css/checkout')
             ->add_js('js/checkout')
             ->set('title', 'Hotmusic - Checkout')
             ->set('cart', $cart)
             ->set('hasPromo', $hasPromo)
             ->set('client', $this->client)
             ->set('full_client', $this->st->client->get(TRUE))
             ->set('resume', $resume)
             ->set('shipping', $shipping)
             ->set('cart_amount', $cart_amount)
             ->set('address', $address)
             ->set('debt_account', $this->st->content->get(array(
                'content' => 'debito-em-conta',
                'limit' => 1
             )))
             ->build('checkout');
    }

    public function finish()
    {
        if (!$this->input->is_ajax_request())
            show_404();

        // Validação
        $this->load->library('form_validation');
        $this->form_validation->set_rules('method', 'Método de Pagamento', 'trim|required');
        if($this->input->post('method') == 'credit_card') {
            $this->form_validation->set_rules('installments', 'Número de Parcelas', 'trim|required');
            $this->form_validation->set_rules('card[brand]', 'Bandeira do Cartão', 'trim|required');
            $this->form_validation->set_rules('card[number]', 'Número do Cartão', 'trim|required');
            $this->form_validation->set_rules('card[name]', 'Nome do Titular', 'trim|required');
            $this->form_validation->set_rules('card[cpf]', 'CPF do titular do cartão', 'trim|required');
            $this->form_validation->set_rules('card[expiration_month]', 'Data de Validade', 'trim|required');
            $this->form_validation->set_rules('card[expiration_year]', 'Data de Validade', 'trim|required');
            $this->form_validation->set_rules('card[cvc]', 'Código de Segurança', 'trim|required');
        }

        if ($this->form_validation->run()) {

            // Retrieves the cart products
            $cart = $this->st->cart->get();
            // Retrieves the cart resume
            $resume = $this->st->cart->get_resume();
            // Retrieves the client info
            $client = $this->st->client->get();
            // Get POST
            $data = $this->input->post();

            // Set data
            $data['products']           = array();
            $data['client']             = $client->id;
            $data['price']              = $resume->total;
            $data['coupon']             = isset($resume->coupon->reference) ? $resume->coupon->reference : FALSE;
            $data['pickup_at_store']    = $resume->pickup_at_store;

            $data['observation'] = '';

            if(!$resume->pickup_at_store){
                $data['shipping']           = $resume->shipping;
                $data['address']            = $resume->address;
            }else{
                $data['shipping']           = 0;
                $data['address']            = FALSE;
                $data['observation']        .= "Método de entrega: Retirar na loja.<br />";
            }

            $hasPromo = FALSE;

            // Prepare products
            foreach ($cart as $key => $product) {
                if ($product->promo)
                    $hasPromo = TRUE;
                $data['products'][] = array(
                    'id'        => $product->id,
                    'quantity'  => $product->quantity,
                    'price'     => $product->price,
                    'variation' => isset($product->variation) ? $product->variation : null
                );
            }

            if($this->input->post('method') == 'credit_card') {
                $data['card']['expiration'] = $data['card']['expiration_month'].'/'.$data['card']['expiration_year'];
                $data['observation'] .= "Método de pagamento: Cartão de crédito<br />";
            } else if($this->input->post('method') == 'bank_transfer') {
                $data['make_transaction'] = FALSE;
                $data['installments'] = 1;
                if (!$hasPromo){
                    $data['percent_discount'] = DEBT_PRICE_DISCOUNT;
                    $data['observation'] .= "Método de pagamento: Transferência bancária" . (DEBT_PRICE_DISCOUNT ? "<br />Desconto de " . DEBT_PRICE_DISCOUNT . "% pelo método de pagamento." : "") . "<br />";
                }
            }

            // Order the cart
            $order = $this->st->orders->add($data);

            if ($order->success){
                $this->st->cart->reset();
                $response = array(
                    'status'=> TRUE,
                    'redirect' => site_url('carrinho/sucesso/'.$order->reference),
                );
            } else {
                $response = array(
                    'status' => FALSE,
                    'reset' => TRUE,
                    'title' => 'Pagamento NÃO efetuado',
                    'message' => $order->message.'. Se estiver com dúvidas, entre em contato.',
                    'class' => 'error',
                );
            }
        } else {
            $error = $this->form_validation->error_array();
            $response = array('status'=> FALSE, 'class' => 'error', 'title' => 'Ocorreu um erro!', 'message' => (count($error) > 1 ? 'Preencha os campos corretamente!' : array_shift($error)), 'fail' => $this->form_validation->error_array(TRUE));
        }

        $this->output->set_content_type('application/json')
                     ->set_output(json_encode($response));
    }

}