<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sucesso extends MY_Controller
{

    public function index($reference = FALSE)
    {
        if (!$reference && !$this->st->client->get())
            redirect('login');

        $order = $this->st->orders->get(array('reference' => $reference, 'client' => $this->st->client->get()->id));

        if (empty($order) || !$order)
            show_404();

        $this->template->set('title', 'Hotmusic - ' . $order->status)
                       ->add_css('css/sucesso')
                       ->set('order', $order)
                       ->set('reference', $reference)
                       ->set('debt_account', $this->st->content->get(array(
                            'content' => 'debito-em-conta',
                            'limit' => 1
                         )))
                       ->build('sucesso');
    }

}