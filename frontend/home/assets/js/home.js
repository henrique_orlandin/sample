
"use strict";

var home;

function Home(){ return this; };

Home.prototype.init = function() {
    this.main_banner();
    this.highlight_gallery();
    this.offers_gallery();
};

Home.prototype.main_banner = function() {
    var items = 1;
    $('.main-banner .banner-list').slick({
        dots: true,
        slidesToShow: items,
        slidesToScroll: items,
        infinite: false,
        speed: 500,
        autoplay: true,
        autoplaySpeed: 5000,
        appendDots: $('.main-banner .dots'),
        prevArrow: $('.main-banner .prev'),
        nextArrow: $('.main-banner .next'),
        responsive: [
            {
                breakpoint: 960,
                settings: {
                    arrows: false
                }
            },
        ]
    }).on('beforeChange', function(event, slick, currentSlide, nextSlide){
        var selector = $('.main-banner .banner-data:eq('+nextSlide+') .lazyload');
        if(!selector.hasClass('loaded'))
            selector.lazyload('load');
    });
    $('.main-banner .banner-list').slick('slickPause');
    $('.main-banner .banner-data.slick-active .lazyload').lazyload({ background:true, onLoad: function (){ $('.main-banner .banner-list').slick('slickPlay'); }}).lazyload('load');
    $('.main-banner .banner-data:not(.slick-active) .lazyload').lazyload({background:true});
};

Home.prototype.highlight_gallery = function() {
    if (!$('.highlights').hasClass('slick'))
        return;
    var timer = '',
        $list = $('.highlights .products-list');
    $(window).on('resize.slickProducts', function() {
        clearTimeout(timer);
        timer = setTimeout(function() {
            if (window.innerWidth >= 960) {
                if ($list.hasClass('slick-initialized')) {
                    $list.slick('unslick');
                }
                return;
            }else if (!$list.hasClass('slick-initialized')) {
                return $list.slick({
                    arrows: false,
                    dots: true,
                    infinite: false,
                    speed: 900,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    responsive: [
                        {
                            breakpoint: 780,
                            settings: {
                                slidesToShow: 2
                            }
                        },
                        {
                            breakpoint: 520,
                            settings: {
                                slidesToShow: 1
                            }
                        }
                    ]
                });
            }
        }, 100);
        if (window.innerWidth >= 960) {
            if ($list.hasClass('slick-initialized')) {
                $list.slick('unslick');
            }
            return;
        }
    }).trigger('resize.slickProducts');
};

Home.prototype.offers_gallery = function() {
    var items = 3;
    $('.offers-gallery .products-list').slick({
        dots: true,
        slidesToShow: items,
        slidesToScroll: items,
        infinite: false,
        speed: 300,
        autoplay: true,
        autoplaySpeed: 5000,
        prevArrow: $('.offers-gallery .prev'),
        nextArrow: $('.offers-gallery .next'),
        appendDots: $('.offers-gallery .common-dots'),
        responsive: [
            {
                breakpoint: 1700,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 1100,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 960,
                dots: true,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 570,
                dots: true,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    }).on('beforeChange', function(event, slick, currentSlide, nextSlide){
        for (var i = 0; i < items; i++) {
            var selector = $('.offers-gallery .product-data:eq('+(nextSlide+i)+') .lazyload');
            if(!selector.hasClass('loaded'))
                selector.lazyload('load');
        }
    });
    $('.offers-gallery .product-data .lazyload').lazyload();
    $('.offers-gallery .product-data.slick-active .lazyload').lazyload('load');
};

$(document).ready(function (){
    home = new Home();
    home.init();
});