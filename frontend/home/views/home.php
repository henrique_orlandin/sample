<main id="home">
    <section class="banners"> <?php /* Add classe full pra remover produtos destaque banner */?>
        <div class="main-banner">
            <ul class="banner-list">
            <?php foreach($banners as $banner) { ?>
                <li class="banner-data">
                    <?php
                    $options = array(
                        'src'           => site_url('image/' . ($_is_mobile ? 'resize' : 'resize_crop') . '?'.($_is_mobile ? 'w=980&h=420' : 'w=980&h=420').'&src='.$banner->image->file),
                        'alt'           => $banner->title,
                        'class'         => 'lazyload ondemand',
                        'data-autoload' => 0,
                    );
                    if ($banner->link){
                        $options['tag']     = 'a';
                        $options['target']  = '_blank';
                        $options['href']    = $banner->link;
                    }
                    echo lazyload($options);
                    ?>
                </li>
            <?php } ?>
            </ul>
            <div class="dots"></div>
            <div class="common-arrows">
                <button class="arrow prev"><i class="fa fa-angle-left"></i></button>
                <button class="arrow next"><i class="fa fa-angle-right"></i></button>
            </div>
        </div>
        <div class="common-advantages">
            <?php $this->load->view('comum/advantages'); ?>
        </div>
        <?php if (!$_is_mobile){ ?>
            <div class="secundary-banner">
                <ul class="banner-list">
                    <?php if($banner_left){  ?>
                        <li class="banner-data">
                            <?php
                            $options = array(
                                'src'               => site_url('image/' . ($_is_mobile ? 'resize' : 'resize_crop') . '?'.($_is_mobile ? 'w=490&h=340' : 'w=490&h=340').'&src='.($banner_left->image ? $banner_left->image->file : '')),
                                'alt'               => $banner_left->title,
                                'class'             => 'lazyload',
                                'data-background'   => 1,
                                'data-viewport'     => 1
                            );
                            if ($banner_left->link){
                                $options['tag']     = 'a';
                                $options['target']  = '_blank';
                                $options['href']    = $banner_left->link;
                            }
                            echo lazyload($options);
                            ?>
                        </li>
                    <?php } if($banner_right){ ?>
                        <li class="banner-data">
                            <?php
                            $options = array(
                                'src'               => site_url('image/' . ($_is_mobile ? 'resize' : 'resize_crop') . '?'.($_is_mobile ? 'w=490&h=340' : 'w=490&h=340').'&src='.($banner_right->image ? $banner_right->image->file : '')),
                                'alt'               => $banner_right->title,
                                'class'             => 'lazyload',
                                'data-background'   => 1,
                                'data-viewport'     => 1
                            );
                            if ($banner_right->link){
                                $options['tag']     = 'a';
                                $options['target']  = '_blank';
                                $options['href']    = $banner_right->link;
                            }
                            echo lazyload($options);
                            ?>
                        </li>
                    <?php } ?>
                </ul>
                <div class="dots"></div>
                <div class="common-arrows reverse">
                    <button class="arrow prev"><i class="fa fa-angle-left"></i></button>
                    <button class="arrow next"><i class="fa fa-angle-right"></i></button>
                </div>
            </div>
        <?php } ?>
    </section>
    <section class="highlights <?php echo !$_is_mobile ? 'slick' : ''; ?>">
        <?php $this->load->view('produtos/listagem', array('products' => $highlighted)); ?>
    </section>
    <section class="offers">
        <div class="intro">
            <?php echo load_svg('sound-waves.svg'); ?>
            <?php if (date('W') == 47){ // BLACK FRIDAY ?>
                <h1 class="title">
                    <span>Black</span> <br/><strong>Friday</strong>
                </h1>
                <p class="desc">Ofertas imperdíveis da Hotmusic para a <strong>Black Friday</strong>!</p>
            <?php } else { ?>
                <h1 class="title">
                    <span>Promo<br/>ções</span> <strong>da<br/>semana</strong>
                </h1>
                <p class="desc">A Hotmusic separa toda semana ofertas <strong>imperdíveis</strong> para você!</p>
            <?php } ?>
        </div>
        <div class="offers-gallery">
            <?php $this->load->view('produtos/listagem', array('products' => $sales)); ?>
            <div class="common-arrows">
                <button class="arrow prev"><i class="fa fa-angle-left"></i></button>
                <button class="arrow next"><i class="fa fa-angle-right"></i></button>
            </div>
            <div class="common-dots"></div>
        </div>
    </section>
</main>