<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

    /**
     * @author Michael Cruz [michael.fernando.cruz@gmail.com]
     * @date   2017-11-05
     */
    public function index()
    {
        //======= ENABLE ST GET REQUEST CACHE
        $this->st->enable_cache();
        //===================================

        $all_banners = $this->st->content->get(array(
            'content' => 'banners'
        ));

        $banners = array();
        $banner_left = FALSE;
        $banner_right = FALSE;

        foreach ($all_banners as $banner) {
            if ($banner->position == 'left') {
                $banner_left = $banner;
            } else if ($banner->position == 'right') {
                $banner_right = $banner;
            }else {
                $banners[] = $banner;
            }
        }

        $highlighted = $this->st->products->get(array('where' => array('highlighted' => true), 'limit' => 4));

        if(empty($highlighted))
            $highlighted = $this->st->products->get();

        $sales = $this->st->products->get(array('promos_only' => true));

        if(empty($sales))
            $sales = $this->st->products->get();

        //======= DISABLE ST GET REQUEST CACHE
        $this->st->disable_cache();
        //===================================

        $json_ld = array(
            "@context" => "http://schema.org/",
            "@type" => "Organization",
            "legalName" => "ALBE MAGNUS COMÉRCIO DE INSTRUMENTOS MUSICAIS LTDA",
            "name" => $this->store->company,
            "image" => base_img('image.png'),
            "logo" => base_img('logo.png'),
            "url" => site_url(),
            "address" => array(
                array(
                    "@type"             => "PostalAddress",
                    "addressCountry"    => "BR",
                    "addressLocality"   => "Caxias do Sul",
                    "addressRegion"     => "RS",
                    "streetAddress"     => 'Rua Coronel Flores, 376, São Pelegrino',
                    "hoursAvailable"    => "Mo,Tu,We,Th,Fr 09:00-19:00 Sa 08:30-12:00"
                ),
                array(
                    "@type"             => "PostalAddress",
                    "addressCountry"    => "BR",
                    "addressLocality"   => "Farroupilha",
                    "addressRegion"     => "RS",
                    "streetAddress"     => 'Rua Tiradentes, 212, Sala 03, Centro',
                    "hoursAvailable"    => "Mo,Tu,We,Th,Fr 09:00-18:00 Sa 09:00-12:00"
                )
            ),
            "telephone" => '+55 (54) 3028.0911',
            "sameAs"        => array(
                "https://www.facebook.com/somoshotmusic",
                "https://www.instagram.com/somoshotmusic"
            ),
            "potentialAction" => array(
                "@type" => "SearchAction",
                "target" => site_url("produtos?q={search_term_string}"),
                "query-input" => "required name=search_term_string"
            )
        );

        $this->template
             // ->add_metadata('fb:app_id', '880970208741655', 'property')
             ->add_metadata('og:locale', 'pt_BR', 'property')
             ->add_metadata('og:url', site_url(), 'property')
             ->add_metadata('og:title', $this->site_title, 'property')
             ->add_metadata('og:site_name', $this->site_title, 'property')
             ->add_metadata('og:type', 'website', 'property')
             ->add_metadata('twitter:card', 'summary')
             ->add_metadata('twitter:site', '@hotmusic')
             ->add_metadata('twitter:creator', '@hotmusic')
             ->add_metadata('canonical', site_url(), 'link')
             ->add_metadata('twitter:title', $this->site_title)
             ->add_json_ld($json_ld)
             ->add_css('css/home')
             ->add_js('js/home')
             ->set('title', 'Hotmusic - Tecnologia em Música')
             ->set('highlighted', $highlighted)
             ->set('sales', $sales)
             ->set('banner_left', $banner_left)
             ->set('banner_right', $banner_right)
             ->set('banners', $banners)
             ->build('home');
    }

}
