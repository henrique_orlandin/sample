<main id="common-pages">

    <section class="common-header">
        <!-- título e breadcrumbs -->
        <div class="page-data">
            <div class="icon desktop-only"><?php echo load_svg('order.svg'); ?></div>
            <div class="text">
                <ul class="breadcrumbs">
                    <li class="breadcrumb-data"><a href="<?php echo site_url(); ?>">Página inicial</a></li>
                    <li class="breadcrumb-data"><strong><?php echo $policy->title; ?></strong></li>
                </ul>
                <div class="icon mobile-only"><?php echo load_svg('order.svg'); ?></div>
                <h1 class="title"><?php echo $policy->title; ?></h1>
            </div>
        </div>

    </section>
    <section class="content">
        <div class="common-text">
            <?php echo $policy->description; ?>
        </div>
    </section>
    <aside class="common-advantages">
        <?php $this->load->view('comum/advantages'); ?>
    </aside>
</main>