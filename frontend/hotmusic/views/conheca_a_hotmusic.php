<main id="about">
    <section class="header">

        <?php
        echo lazyload(array(
            'src'   => resize_url('image/resize?w=1649&h=424&src='.base_img('about-top.png')),
            'alt'   => $about->title.' '.$about->subtitle,
            'class' => 'lazyload',
            'data-background' => 1
        )); ?>
        <h1 class="title">
            <?php echo $about->title; ?> <strong><?php echo $about->subtitle; ?></strong>
        </h1>

    </section>

    <section class="content">
        <div class="common-text"><?php echo $about->description; ?></div>
        <div class="common-gallery">
            <ul class="gallery-list">
                <?php foreach($about->gallery as $gallery) { ?>
                <li class="gallery-data">
                    <?php
                    echo lazyload(array(
                        'src'   => resize_url('image/resize?w=760&h=460&src='.$gallery->path),
                        'alt'   => $about->title.' '.$about->subtitle,
                        'title'   => $about->title.' '.$about->subtitle,
                        'rel'   => 'gallery',
                        'class' => 'lazyload image colorbox',
                        'tag'   => 'a',
                        'href'  => resize_url('image/resize?w=1250&h=900&src='.$gallery->path),
                        'data-autoload' => false,
                    )); ?>
                </li>
                <?php } ?>
            </ul>
            <div class="common-dots"></div>
            <div class="common-arrows">
                <button class="arrow prev"><i class="fa fa-angle-left"></i></button>
                <button class="arrow next"><i class="fa fa-angle-right"></i></button>
            </div>
        </div>
        <div class="contact-info">
            <div class="left">
                <ul class="contact-list">
                    <?php if ($store->phone_alternative){ ?>
                        <li class="contact-data">
                            <a class="link whats" href="tel:<?php echo preg_replace('/\D/', '', $store->phone_alternative); ?>">
                                <span class="icon"><?php echo load_svg('whatsapp.svg'); ?></span>
                                <strong class="number"><?php echo $store->phone_alternative; ?></strong>
                            </a>
                        </li>
                    <?php } ?>
                    <li class="contact-data">
                        <a class="link phone" href="tel:<?php echo preg_replace('/\D/', '', $store->phone); ?>">
                            <span class="icon"><?php echo load_svg('phone.svg'); ?></span>
                            <strong class="number"><?php echo $store->phone; ?></strong>
                        </a>
                    </li>
                    <li class="contact-data">
                        <a class="link email" href="mailTo:<?php echo $store->email; ?>">
                            <span class="icon"><?php echo load_svg('email.svg'); ?></span>
                            <strong class="text"><?php echo $store->email; ?></strong>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="right">
                <h5 class="address-title">Endereço</h5>
                <address class="address"><?php echo $store->address; ?></address>
                <a href="https://goo.gl/TRA8Y6" class="address-link" target="_blank">Veja como Chegar</a>
            </div>
        </div>
    </section>
    <aside class="company-image desktop-only">
        <?php
        echo lazyload(array(
            'src'   => resize_url('image/resize?w=670&h=713&src='.$about->image->file),
            'alt'   => $about->title.' '.$about->subtitle,
            'class' => 'lazyload',
            'tag'   => 'figure',
            'data-viewport' => false,
        )); ?>
    </aside>
    <aside class="common-advantages">
        <?php $this->load->view('comum/advantages'); ?>
    </aside>
</main>
