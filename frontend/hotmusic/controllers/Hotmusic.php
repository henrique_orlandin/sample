<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Hotmusic extends MY_Controller
{

    public function index()
    {
        //======= ENABLE ST GET REQUEST CACHE
        $this->st->enable_cache();
        //===================================
        $data = $this->st->content->get(array(
            'content' => 'about_us',
            'gallery' => TRUE,
            'limit' => 1
        ));

        $this->template
             ->set('title', 'Hotmusic - Conheça a Hotmusic')
             ->add_css('css/conheca_a_hotmusic')
             ->add_js('js/conheca_a_hotmusic')
             ->set('about', $data)
             ->build('conheca_a_hotmusic');
    }

    public function trocas_e_devolucoes()
    {
        //======= ENABLE ST GET REQUEST CACHE
        $this->st->enable_cache();
        //===================================
        $data = $this->st->content->get(array(
            'content' => 'trocas-e-devolucoes',
            'limit' => 1
        ));
        //======= DISABLE ST GET REQUEST CACHE
        $this->st->disable_cache();
        //===================================

        $this->template
             ->set('title', 'Hotmusic - Trocas e Devoluções')
             ->add_css('css/common_pages')
             ->add_js('js/common_pages')
             ->set('policy', $data)
             ->build('trocas_e_devolucoes');
    }

    public function informacoes_de_entrega()
    {
        //======= ENABLE ST GET REQUEST CACHE
        $this->st->enable_cache();
        //===================================
        $data = $this->st->content->get(array(
            'content' => 'informacoes-de-entrega',
            'limit' => 1
        ));
        //======= DISABLE ST GET REQUEST CACHE
        $this->st->disable_cache();
        //===================================

        $this->template
             ->set('title', 'Hotmusic - Informações de Entrega')
             ->add_css('css/common_pages')
             ->add_js('js/common_pages')
             ->set('terms', $data)
             ->build('informacoes_de_entrega');
    }

}
