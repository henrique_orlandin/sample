
"use strict";

function Conheca_a_hotmusic(){ this.init(); };

Conheca_a_hotmusic.prototype.init = function() {

    this.gallery();
    app.initColorbox();

};

Conheca_a_hotmusic.prototype.gallery = function() {
    var items = 1;
    $('.common-gallery .gallery-list').slick({
        dots: true,
        slidesToShow: items,
        slidesToScroll: items,
        infinite: false,
        speed: 500,
        autoplay: true,
        autoplaySpeed: 5000,
        appendDots: $('.common-gallery .common-dots'),
        prevArrow: $('.common-gallery .prev'),
        nextArrow: $('.common-gallery .next'),
        responsive: [
            {
                breakpoint: 960,
                settings: {
                    arrows: false
                }
            },
        ]
    }).on('beforeChange', function(event, slick, currentSlide, nextSlide){
        var selector = $('.common-gallery .gallery-data:eq('+nextSlide+') .lazyload');
        if(!selector.hasClass('loaded'))
            selector.lazyload('load');
    });
    $('.common-gallery .gallery-list').slick('slickPause');
    $('.common-gallery .gallery-data.slick-active .lazyload').lazyload({ background:true, onLoad: function (){ $('.common-gallery .gallery-list').slick('slickPlay'); }}).lazyload('load');
    $('.common-gallery .gallery-data:not(.slick-active) .lazyload').lazyload({background:true});
};

$(document).ready(function (){
    new Conheca_a_hotmusic();
});