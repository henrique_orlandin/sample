<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Comum extends MY_Controller
{
    public function index()
    {
        set_status_header(404);
        $this->template->set('title', 'Hotmusic - Página não encontrada')
                       ->add_css('css/error_404', 'comum')
                       ->build('comum/error_404');
    }

    public function get_cities()
    {
        if (!$this->input->is_ajax_request())
            show_404();

        //======= ENABLE ST GET REQUEST CACHE
        $this->st->enable_cache();
        //===================================

        $this->st->utils->get_cities($this->input->post('id'), TRUE);

        //======= DISABLE ST GET REQUEST CACHE
        $this->st->disable_cache();
        //===================================
    }

    public function get_states()
    {
        if (!$this->input->is_ajax_request())
            show_404();

        //======= ENABLE ST GET REQUEST CACHE
        $this->st->enable_cache();
        //===================================

        $this->st->utils->get_states(TRUE);

        //======= DISABLE ST GET REQUEST CACHE
        $this->st->disable_cache();
        //===================================
    }

    public function get_cep()
    {
        if (!$this->input->is_ajax_request())
            show_404();

        //======= ENABLE ST GET REQUEST CACHE
        $this->st->enable_cache();
        //===================================

        $this->st->utils->get_address($this->input->post('cep'), TRUE);

        //======= DISABLE ST GET REQUEST CACHE
        $this->st->disable_cache();
        //===================================
    }
}