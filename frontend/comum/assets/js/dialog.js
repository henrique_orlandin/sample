"use strict";

function Dialog() {
    this.init();
};

Dialog.prototype.init = function(){
    if (this.initialized)
        return;
    this.initialized = true;
    $('body').append('\
        <div id="dialog-overflow"></div>\
        <div id="dialog-window">\
            <div class="dialog-message">\
                <button type="button" id="close-dialog"></button>\
                <div class="icon-message">\
                    <div class="alert">\
                        <i class="fa fa-exclamation-triangle"></i>\
                    </div>\
                    <div class="success">\
                        <i class="fa fa-check"></i>\
                    </div>\
                    <div class="error">\
                        <i class="fa fa-times"></i>\
                    </div>\
                </div>\
                <p class="title"></p>\
                <div class="message"></div>\
            </div>\
            <div class="dialog-buttons">\
                <button type="button" id="cancel-dialog">Cancelar</button>\
                <button type="button" id="confirm-dialog"></button>\
            </div>\
        </div>\
    ');
    $('#close-dialog, #cancel-dialog').on('click', $.proxy(function(){
        this.close();
    }, this));
    $('#dialog-overflow').on('click', $.proxy(function(){
        if ($('body').hasClass('dialog-open')){
            this.close();
        }
    }, this));
};
Dialog.prototype.open = function(options){
    // Reset dialog
    var defaults = {
        class: 'success',
        button: 'Fechar',
        cancel: 'Cancelar',
        title: '',
        message: '',
        cancel: false,
        callback: function(){},
        autoClose: true
    }, data =  $.extend( {}, defaults, options );
    var $dialog = $('#dialog-window');
    $dialog.removeClass('error success alert').addClass(data.class).find('.message').html(data.message);
    $dialog.find('.title').html(data.title);
    $dialog.find('#cancel-dialog').html(data.button);
    $dialog.find('#confirm-dialog').html(data.button).focus().on('click', $.proxy(function(){
        if (typeof data.callback == 'function') {
            data.callback();
        }
        if (data.autoClose)
            this.close();
    }, this));
    if (data.cancel){
        $dialog.find('#cancel-dialog').show();
        var w = 0;
        $dialog.find('.dialog-buttons button').each(function(){
            w = Math.max(w, $(this).width());
        }).width(w);
    }
    else
        $dialog.find('#cancel-dialog').hide();
    // Open Dialog
    $('body').addClass('dialog-open');
};
Dialog.prototype.close = function(){
    $('#dialog-window').find('#confirm-dialog').off('click');
    // Close Dialog
    $('body').removeClass('dialog-open');
};