"use strict";

function History() {
    this.init();
};

// Common Vars
History.prototype.block         = false;
History.prototype.changeHash    = false;
History.prototype.first         = true;
History.prototype.initialURL    = location.href;
History.prototype.hasHistory    = Modernizr.history;
History.prototype.pushed        = true;
History.prototype.save          = true;
History.prototype.states        = [];
History.prototype.url           = '';

History.prototype.init = function(){
    this.pushHistory({
        title: document.title,
        url: segments.join('/')+document.location.search,
    });
};

History.prototype.pushHistory = function(params){
    var self = this;
    if (!self.block){
        if(self.hasHistory){
            if (self.first){
                self.replaceHistory(params);
                self.first = false;
                return true;
            }
            if (history.state == null || history.state.url != params.url){
                if (self.save){
                    history.pushState(params, false, base_url + params.url);
                    self.states.push(params);
                    if (params.title !== undefined)
                        document.title = params.title;
                }
            }
            self.save = true;
        }else{
            if (self.save){
                self.changeHash = true;
                window.location.hash = '#/' + params.url;
                self.states.push(params);
                setTimeout(function(){
                    self.changeHash = false;
                },100);
                if (params.title !== undefined)
                    document.title = params.title;
            }else{
                setTimeout(function(){
                    self.save = true;
                    self.changeHash = false;
                },100);
            }
        }
    }
};
History.prototype.popHistory = function(callback){
    var self = this;
    if(self.hasHistory){
        window.onpopstate = function(e){
            e.preventDefault();
            // Solution for auto popState from Chrome
            var onloadPop = !self.pushed && location.href == self.initialURL;
            self.pushed = true;
            if (onloadPop) return;
            // Do the magic
            if (e.state){
                var params = self.states.pop();
                if (typeof callback == 'function') {
                    self.save   = false;
                    self.url    = e.state.url.replace(base_url,'');
                    if (e.state.title !== undefined)
                        document.title = e.state.title;
                    return callback(self.url);
                }
            }
        };
    }else{
        window.onhashchange = function(){
            if (!self.changeHash){
                var params = self.states.pop();
                if (params !== undefined && params.url == self.url)
                    self.states.push(params);
                if (typeof callback == 'function') {
                    self.save   = false;
                    self.url    = (location.hash).replace('#/','');
                    if (params.title !== undefined)
                        document.title = params.title;
                    return callback(self.url);
                }
            }
        };
    }
};
History.prototype.replaceHistory = function(params){
    var self = this;
    if(self.hasHistory)
        history.replaceState(params, false, base_url + params.url);
    self.states[self.states.length - 1] = params;
    if (params.title !== undefined)
        document.title = params.title;
};
History.prototype.lock = function(){
    var self = this;
    self.block = true;
};
History.prototype.unlock = function(){
    var self = this;
    self.block = false;
};
