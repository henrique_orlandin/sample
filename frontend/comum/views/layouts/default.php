<!DOCTYPE html>
<html lang="pt-br" <?php echo $_is_mobile ? 'class="mobile"' : 'class="desktop"'; ?>>
    <head>
        <base href="<?php echo base_url(); ?>" >
        <meta charset="utf-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta http-equiv="cleartype" content="on">
        <meta name="theme-color" content="#222222" />
        <meta name="msapplication-TileColor" content="#222222">
        <meta name="HandheldFriendly" content="True">
        <meta name="MobileOptimized" content="320">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="format-detection" content="telephone=no">

        <title><?php echo $title; ?></title>
        <?php echo $metadata; ?>
        <meta name="author" content="StoreTrooper">
        <meta name="robots" content="index, follow">
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_img('icon/favicon.ico'); ?>">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_img('icon/apple-touch-icon.png'); ?>">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_img('icon/favicon-32x32.png'); ?>">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_img('icon/favicon-16x16.png'); ?>">
        <link rel="manifest" href="<?php echo base_img('icon/manifest.json'); ?>">
        <link rel="mask-icon" href="<?php echo base_img('icon/safari-pinned-tab.svg'); ?>" color="#d52027">

        <?php echo $head_styles; ?>
        <?php echo $head_scripts; ?>

        <!-- ADWORDS -->
        <!-- Global site tag (gtag.js) - AdWords: 848834015 -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=AW-848834015"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'AW-848834015');
        </script>
        <!-- END ADWORDS -->

        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '981696362001366');
            fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=981696362001366&ev=PageView&noscript=1" /></noscript>
        <!-- End Facebook Pixel Code -->

        <?php if($_module == 'produtos' && isset($product_details) && $product_details){ ?>
        <!-- ADWORDS DETALHAMENTO PRODUTO -->
        <script>
            gtag('event', 'page_view', {'send_to': 'AW-848834015 ',
                'ecomm_prodid': '<?php echo $product->id; ?>',
                'ecomm_pagetype': '<?php echo $product->category; ?>',
                'ecomm_totalvalue': '<?php echo (float) $product->price; ?>'
            });
        </script>
        <!-- END ADWORDS DETALHAMENTO PRODUTO -->
        <?php } ?>

        <!-- ANALYTICS -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-75132972-7"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-75132972-7');
        ga("create", "UA-75132972-7");
        ga('require', 'ec');
        ga('set', 'currencyCode', 'BRL');
        </script>
        <!-- END ANALYTICS -->

    </head>
    <body class="preload">
        <section id="wrapper">
            <?php echo $header, $navigation; ?>
            <section id="content">
                <?php echo $breadcrumb, $content, $footer; ?>
            </section>
            <?php echo $cart_alert; ?>
        </section>

        <!-- parcelamento do produto -->
        <?php if(isset($installments) && !empty($installments) && $_module == 'produtos'){ ?>
            <aside id="payment-conditions">
                <div class="overlay"></div>
                <div class="content-box">
                    <button class="common-button"><?php echo load_svg('cancel.svg'); ?></button>
                    <h5 class="title">Veja todas as Parcelas</h5>
                    <ul class="conditions-list">
                        <?php $this->load->view('comum/_parcelas'); ?>
                    </ul>
                </div>
            </aside>
        <?php } ?>

        <!-- debito em conta do produto -->
        <?php if(isset($debt_account) && !empty($debt_account) && $_module == 'produtos'){ ?>
            <aside id="debt-account-modal">
                <div class="overlay"></div>
                <div class="content-box">
                    <button class="common-button"><?php echo load_svg('cancel.svg'); ?></button>
                    <h5 class="title">Informações para depósito bancário</h5>
                    <div class="common-text"><?php echo $debt_account->description; ?></div>
                </div>
            </aside>
        <?php } ?>

        <script type="text/javascript">
            var base_url = '<?php echo base_url(); ?>',
                base_img = '<?php echo base_img(); ?>',
                base_js = '<?php echo base_js(); ?>',
                segments = '<?php echo trim($this->uri->uri_string(),"/"); ?>'.split('/'),
                is_mobile = <?php echo $_is_mobile ? 'true' : 'false'; ?>;
        </script>
        <?php echo $body_scripts; ?>

        <!-- MESSENGER -->
        <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId: '880970208741655',
                autoLogAppEvents: true,
                xfbml: true,
                version: 'v2.11'
            });
        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        </script>
        <div class="fb-customerchat"
            page_id="795005230614656"
            minimized="true"
            logged_in_greeting="Se precisar falar com a gente, estamos aqui (:"
            logged_out_greeting="Se precisar falar com a gente, estamos aqui (:"
            theme_color="#D52027"
        >
        </div>
        <!-- END MESSENGER -->
    </body>
</html>
