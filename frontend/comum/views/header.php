<header id="header">

    <button class="toggle-menu mobile-only">
        <span></span>
        <span></span>
        <span></span>
    </button>

    <div class="logo">
        <a href="<?php echo site_url(); ?>" title="Hotmusic">
            <img src="<?php echo base_img('logo.png'); ?>" alt="Hotmusic">
        </a>
    </div>

    <div class="contact desktop-only">
        <ul class="contact-list">

            <li class="contact-data phone">
                <div class="align">
                    <span class="icon"><?php echo load_svg('phone.svg'); ?></span>
                    <strong class="number"><?php echo $store->phone; ?></strong>
                </div>
            </li>

            <?php if ($store->phone_alternative){ ?>
                <li class="contact-data whatsapp">
                    <div class="align">
                        <span class="icon"><?php echo load_svg('whatsapp.svg'); ?></span>
                        <strong class="number"><?php echo $store->phone_alternative; ?></strong>
                    </div>
                </li>
            <?php } ?>

        </ul>

        <div class="facebook">
            <a class="align" href="https://www.facebook.com/somoshotmusic/?pnref=story" target="_blank">
                <?php echo load_svg('facebook.svg'); ?>
            </a>
        </div>

        <div class="instagram">
            <a class="align" href="https://www.instagram.com/somoshotmusic/" target="_blank">
                <?php echo load_svg('instagram.svg'); ?>
            </a>
        </div>

    </div>

    <div class="right">

        <div class="whats mobile-only">

            <button class="toggle-whats">
                <?php echo load_svg('whatsapp.svg'); ?>
            </button>

            <a class="whats-mobile mobile-only" href="tel:<?php echo preg_replace('/\D/', '', $store->phone_alternative); ?>">
                <p class="text">está com dúvidas? <strong>Fale conosco agora mesmo!</strong></p>
                <strong class="number"><?php echo $store->phone_alternative; ?></strong>
            </a>

        </div>

        <div class="search animation-element">

            <button class="toggle-search mobile-only">
                <?php echo load_svg('search.svg'); ?>
            </button>

            <form action="<?php echo site_url('produtos'); ?>" method="get" class="common-form" id="search-form">
                <input type="search" name="q" id="search" placeholder="O que você procura?">
                <button type="submit"><?php echo load_svg('search.svg'); ?></button>
            </form>

        </div>

        <div class="access animation-element" id="access">

            <?php if (!$client) { ?>
            <a class="access-link" href="<?php echo site_url('login'); ?>">
                <?php echo load_svg('user.svg'); ?>
                <strong class="desktop-only">Entrar/Cadastre-se</strong>
            </a>

            <?php } else { ?>

            <button class="toggle-user mobile-only">
                <?php echo load_svg('user.svg'); ?>
            </button>

            <div class="user-content">

                <button class="access-link">
                    <span class="avatar">
                        <?php if(!empty($client->picture)) {
                            $options = array(
                                'src'               => $client->picture,
                                'alt'               => $client->name,
                                'class'             => 'lazyload',
                                'tag'               => 'figure',
                                'data-background'   => 1,
                            );
                            echo lazyload($options);
                        } else {
                            echo load_svg('user.svg');
                        } ?>
                    </span>
                    <strong>Olá, <?php echo $client->name; ?></strong>
                    <i class="fa fa-angle-down desktop-only"></i>
                </button>

                <nav class="user-menu">
                    <ul class="menu-list">
                        <li class="menu-data">
                            <h5><a href="<?php echo site_url('minha-conta/meus-pedidos'); ?>" class="user-menu-link">Meus Pedidos</a></h5>
                        </li>
                        <li class="menu-data">
                            <h5><a href="<?php echo site_url('minha-conta/meus-dados'); ?>" class="user-menu-link">Meus Dados</a></h5>
                        </li>
                        <li class="menu-data">
                            <h5><a href="<?php echo site_url('minha-conta/meus-enderecos'); ?>" class="user-menu-link">Meus Endereços</a></h5>
                        </li>
                        <li class="menu-data">
                            <h5><a href="<?php echo site_url('minha-conta/alterar-senha'); ?>" class="user-menu-link">Alterar Senha</a></h5>
                        </li>
                    </ul>
                    <a href="<?php echo site_url('logout'); ?>" class="exit">
                        <?php echo load_svg('logout.svg'); ?>
                        <span>Sair</span>
                    </a>
                </nav>
            </div>
            <?php } ?>

        </div>

        <div class="cart">
            <a class="cart-link<?php echo $total_cart ? ' active' : ''; ?>" href="<?php echo site_url('carrinho'); ?>">
                <?php echo load_svg('cart.svg'); ?>
                <span id="cart-quantity"><?php echo $total_cart; ?></span>
            </a>
        </div>

    </div>

</header>