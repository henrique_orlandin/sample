<main id="not-found" class="common-limiter">
    <div class="error-title">OPS!</div>
    <div class="error-text">A página que você procura <br>não pôde ser encontrada!</div>
    <form id="error-search" method="GET" action="<?php echo site_url('produtos'); ?>" class="common-form">
        <input type="text" name="q" placeholder="Faça uma nova busca">
        <button type="submit" class="common-button"><i class="fa fa-search"></i></button>
    </form>
    <div class="error-link">ou</div>
    <a href="<?php echo site_url('home') ?>" class="common-button"><span>volte para a home</span></a>
</main>