<ul class="advantage-list">
    <li class="advantage-data">
        <span class="icon"><?php echo load_svg('credit-card.svg'); ?></span>
        <span class="desc">Parcelamento sem juros</span>
    </li>
    <li class="advantage-data shipping">
        <span class="icon"><?php echo load_svg('delivery-truck.svg'); ?></span>
        <span class="desc">Frete Grátis - Consulte<span> Regras e Regiões</span></span>
    </li>
    <li class="advantage-data">
        <span class="icon"><?php echo load_svg('secure-shield.svg'); ?></span>
        <span class="desc">Compra 100% Segura</span>
    </li>
    <li class="advantage-data">
        <span class="icon"><?php echo load_svg('placeholder.svg'); ?></span>
        <span class="desc">Retirada na Loja</span>
    </li>
    <!-- <li class="advantage-data">
        <span class="icon"><?php echo load_svg('barcode.svg'); ?></span>
        <span class="desc">Descontos no Boleto</span>
    </li> -->
</ul>