<footer id="footer">

    <div class="newsletter">

        <div class="footer-limiter">
            <div class="intro">
                <?php echo load_svg('email.svg'); ?>
                <span class="desc">
                    Receba nossas
                    <strong>Promoções em seu e-mail</strong>
                </span>
            </div>
            <div class="intro">
                <?php echo load_svg('email.svg'); ?>
                <span class="desc">
                    Receba nossas
                    <strong>Promoções em seu e-mail</strong>
                </span>
            </div>
            <form action="<?php echo site_url('newsletter/send'); ?>" class="common-form ajax-form" id="news-form" method="post" novalidate>
                <div class="row">
                    <div class="form-group">
                        <input type="text" name="name" placeholder="Nome" required />
                    </div>
                    <div class="form-group">
                        <input type="email" name="email" placeholder="E-mail" required />
                        <button type="submit" class="common-button">
                            <?php echo load_svg('left-arrow.svg'); ?>
                            <?php $this->load->view('comum/preloader'); ?>
                        </button>
                    </div>
                </div>
            </form>
        </div>

    </div>

    <div class="company-info">

        <div class="footer-limiter">
            <div class="contact">
                <h5 class="footer-title">Atendimento</h5>
                <address class="address"><?php echo $store->address; ?></address>
                <ul class="contact-list">
                    <?php if ($store->phone_alternative){ ?>
                        <li class="contact-data">
                            <a class="link whats" href="tel:<?php echo preg_replace('/\D/', '', $store->phone_alternative); ?>">
                                <span class="icon"><?php echo load_svg('whatsapp.svg'); ?></span>
                                <strong class="number"><?php echo $store->phone_alternative; ?></strong>
                            </a>
                        </li>
                    <?php } ?>
                    <li class="contact-data">
                        <a class="link phone" href="tel:<?php echo preg_replace('/\D/', '', $store->phone); ?>">
                            <span class="icon"><?php echo load_svg('phone.svg'); ?></span>
                            <strong class="number"><?php echo $store->phone; ?></strong>
                        </a>
                    </li>
                    <li class="contact-data">
                        <a class="link email" href="mailTo:<?php echo $store->email; ?>">
                            <span class="icon"><?php echo load_svg('email.svg'); ?></span>
                            <strong class="text"><?php echo $store->email; ?></strong>
                        </a>
                    </li>
                </ul>
            </div>

            <div class="sitemap desktop-only">
                <h5 class="footer-title">Informações</h5>
                <ul class="menu-list">
                    <li class="menu-data"><h6><a class="link" href="<?php echo site_url('hotmusic'); ?>">Conheça a Hotmusic</a></h6></li>
                    <li class="menu-data"><h6><a class="link" href="<?php echo $client ? site_url('minha-conta/meus-pedidos') : site_url('login'); ?>">Meus Pedidos</a></h6></li>
                    <li class="menu-data"><h6><a class="link" href="<?php echo site_url('hotmusic/informacoes-de-entrega'); ?>">Informações de Entrega</a></h6></li>
                    <li class="menu-data"><h6><a class="link" href="<?php echo site_url('hotmusic/trocas-e-devolucoes'); ?>">Trocas e Devoluções</a></h6></li>
                    <li class="menu-data"><h6><a class="link" href="<?php echo site_url('contato'); ?>">Contato</a></h6></li>
                </ul>
            </div>

            <div class="service">
                <h5 class="footer-title">Horário de Atendimento</h5>
                <p class="desc"><?php echo $hour_info->title; ?></p>
                <a class="facebook" href="https://www.facebook.com/somoshotmusic/?pnref=story" target="_blank">
                    <span class="icon"><?php echo load_svg('facebook.svg'); ?></span>
                    <strong class="like">Curta nossa Página</strong>
                </a>
                <a class="instagram" href="https://www.instagram.com/somoshotmusic/" target="_blank">
                    <span class="icon"><?php echo load_svg('instagram.svg'); ?></span>
                    <strong class="like">Siga-nos no Instagram</strong>
                </a>
            </div>
        </div>

    </div>

    <div class="site-info">

        <div class="footer-limiter">
            <div class="payment">

                <h5 class="footer-title">Formas de Pagamento</h5>
                <div class="cards">
                    <h6 class="sub-title">Cartões de Crédito</h6>
                    <ul class="flag-list">
                        <li class="flag-data"><img src="<?php echo resize_url('image/resize?w=42&h=26&src='.base_img('flag-visa.png')); ?>" alt="Visa"></li>
                        <li class="flag-data"><img src="<?php echo resize_url('image/resize?w=42&h=26&src='.base_img('flag-mastercard.png')); ?>" alt="MasterCard"></li>
                        <li class="flag-data"><img src="<?php echo resize_url('image/resize?w=42&h=26&src='.base_img('flag-amex.png')); ?>" alt="Amex"></li>
                        <li class="flag-data"><img src="<?php echo resize_url('image/resize?w=42&h=26&src='.base_img('flag-diners.png')); ?>" alt="Diners Club"></li>
                        <li class="flag-data"><img src="<?php echo resize_url('image/resize?w=42&h=26&src='.base_img('flag-elo.png')); ?>" alt="Elo"></li>
                        <!-- <li class="flag-data"><img src="<?php echo resize_url('image/resize?w=42&h=26&src='.base_img('flag-hipercard.png')); ?>" alt="Hipercard"></li> -->
                    </ul>
                </div>
                <!-- <div class="slip">
                    <h6 class="sub-title">Boleto</h6>
                    <div class="slip-img"><img src="<?php echo resize_url('image/resize?w=42&h=26&src='.base_img('slip.png')); ?>" alt="Boleto"></div>
                </div> -->

            </div>

            <div class="certificate">

                <h5 class="footer-title">Certificados</h5>
                <ul class="certificate-list">
                    <li class="certificate-data">
                        <a href="https://www.cloudflare.com/br/" target="_blank">
                            <img src="<?php echo resize_url('image/resize?w=100&h=28&src='.base_img('cloudflare.png')); ?>" alt="Site Blindado">
                        </a>
                    </li>
                    <li class="certificate-data">
                        <a target="_blank">
                            <img src="<?php echo resize_url('image/resize?w=99&h=34&src='.base_img('websiteseguro.png')); ?>" alt="Clear Sale">
                        </a>
                    </li>
                </ul>

            </div>
        </div>

    </div>

    <div class="final-info">

        <div class="text desktop-only">
            <p>Todas as regras e promoções são válidas apenas para produtos vendidos e entregues pela hotmusiccaxias.com.br.</p>
            <p>O preço válido será o da finalização da compra.</p>
            <p>CNPJ: <?php echo format_mask($store->cnpj, '##.###.###/####-##'); ?> / IE: <?php echo $store->ie; ?> / Endereço: <?php echo $store->address; ?>. CEP: <?php echo $store->zipcode; ?></p>
            <p>Atendimento ao Cliente: <a href="mailTo:<?php echo $store->email; ?>"><?php echo $store->email; ?></a></p>
        </div>

        <div class="storetrooper">
            <a href="https://storetrooper.rocks/" target="_blank" title="Storetrooper">
                <img src="<?php echo resize_url('image/resize?w=130&h=35&src='.base_img('storetrooper.png')); ?>" alt="Storetrooper" />
            </a>
        </div>

    </div>

</footer>