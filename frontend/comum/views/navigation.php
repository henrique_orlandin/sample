<section id="navigation" class="animation-element">
    <nav class="menu" id="menu">
        <ul class="menu-list">

            <?php foreach ($categories as $key => $category) { ?>
                <li class="menu-data <?php echo ($this->uri->segment(2) == $category->slug) ? 'current' : ''; ?> animation-element">
                    <?php if(!empty($category->children)){ ?>

                        <button class="link">
                            <span class="icon"><img src="<?php echo $category->image; ?>" alt="<?php echo $category->title; ?>"></span>
                            <h2 class="title"><?php echo $category->title; ?></h2>
                            <i class="fa fa-angle-right"></i>
                        </button>
                        <div class="submenu">
                            <strong class="category desktop-only"><?php echo $category->title; ?></strong>
                            <button class="submenu-back mobile-only">
                                <i class="fa fa-angle-left"></i>
                                <span>Voltar</span>
                            </button>
                            <ul class="submenu-list">
                                <?php foreach ($category->children as $c => $children) { ?>
                                    <li class="submenu-data">
                                        <h3><a class="sub-title" href="<?php echo site_url('produtos/'.$children->slug); ?>"><?php echo $children->title; ?></a></h3>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    <?php }else{ ?>
                        <a href="<?php echo site_url('produtos/'.$category->slug); ?>" class="link">
                            <span class="icon"><img src="<?php echo $category->image; ?>" alt="<?php echo $category->title; ?>"></span>
                            <h2 class="title"><?php echo $category->title; ?></h2>
                        </a>
                    <?php } ?>
                </li>
            <?php } ?>

        </ul>
        <ul class="menu-list menu-info mobile-only">
            <li class="menu-data"><h6><a class="link" href="<?php echo site_url('hotmusic'); ?>">Conheça a Hotmusic</a></h6></li>
            <li class="menu-data"><h6><a class="link" href="<?php echo site_url('minha-conta/meus-pedidos'); ?>">Meus Pedidos</a></h6></li>
            <li class="menu-data"><h6><a class="link" href="<?php echo site_url('hotmusic/informacoes-de-entrega'); ?>">Informações de Entrega</a></h6></li>
            <li class="menu-data"><h6><a class="link" href="<?php echo site_url('hotmusic/trocas-e-devolucoes'); ?>">Trocas e Devoluções</a></h6></li>
            <li class="menu-data"><h6><a class="link" href="<?php echo site_url('contato'); ?>">Contato</a></h6></li>
        </ul>
    </nav>
</section>