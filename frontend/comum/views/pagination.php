<?php if($total > 1) { ?>
<div class="common-pagination">
    <?php echo $pagination; ?>
    <form action="<?php echo site_url($this->uri->uri_string()); ?>" method="get" class="common-form" id="pagination-form">
        <label>Ir para a página</label>
        <input type="number" name="pg" class="mask-pagination" max="<?php echo $total; ?>" />
        <button type="submit" class="common-button"><i class="fa fa-angle-right"></i></button>
    </form>
</div>
<?php } ?>