<?php for ($i = 1; $i <= MAX_INSTALLMENTS; $i++){ ?>
    <?php if ($product->price / $i > MIN_INSTALLMENT_PRICE){ ?>
        <li class="conditions-data">
            <div class="installments"><?php echo $i; ?>x</div>
            <div class="price">de R$ <?php echo mysql_decimal_to_number( bcdiv($product->price, $i, 2) ); ?></div>
            <div class="rate">sem juros</div>
            <div class="total">total R$ <?php echo mysql_decimal_to_number($product->price); ?></div>
        </li>
    <?php } ?>
<?php } ?>