<?php if (isset($route) && !empty($route)){ ?>
<div id="breadcrumb">
    <ul class="common-limiter">
        <li>
            <a href="<?php echo site_url(); ?>" class="undline-hover">
                <span>Home</span>
            </a>
        </li>
        <?php foreach ($route as $link => $title){ ?>
        <li>
            <a href="<?php echo site_url(($link ? $link : $this->uri->uri_string())); ?>" class="undline-hover">
                <span><?php echo $title; ?></span>
            </a>
        </li>
        <?php } ?>
    </ul>
</div>
<?php } ?>