
"use strict";

function Meus_dados(){ this.init(); };

Meus_dados.prototype.init = function() {

    this.form_settings();

};

Meus_dados.prototype.form_settings = function() {


    //validações para campos específicos
    if ($('#change-confirm').length){
        $('#change-confirm').rules( "add", {
            equalTo: '#change-password'
        });
    }

    if ($('#new-confirm').length){
        $('#new-confirm').rules( "add", {
            equalTo: '#new-password'
        });
    }

    $.validator.addMethod("cpf", function(value, element) {
      return this.optional(element) || validate_cpf(value);
    }, "O CPF informado não está correto!");

    $.validator.addMethod("cnpj", function(value, element) {
      return this.optional(element) || validate_cnpj(value);
    }, "O CNPJ informado não está correto!");

    //troca de pessoa física e jurídica
    $('input[name="type"]').on('change',function () {
        var input_value = $('#account-doc').val().replace(/[^\d]+/g,'');
        $('#account-doc').val(input_value);
        if($('input[name="type"]:checked').val() == 'pf') {
            $('#select-gender, #account-birthdate').attr('required','required').rules( "add", { required: true });
            $('#select-gender, #account-birthdate').closest('.row').removeClass('hide');
            $('#account-doc').attr('placeholder', 'CPF').removeClass('error').mask('999.999.999-99').rules( "add", {cpf: true});
            $('#account-doc').rules( "remove", "cnpj");
        } else {
            $('#select-gender, #account-birthdate').removeAttr('required').rules( "remove", "required");
            $('#select-gender, #account-birthdate').closest('.row').addClass('hide');
            $('#account-doc').attr('placeholder', 'CNPJ').removeClass('error').mask('99.999.999/9999-99').rules( "add", {cnpj: true});
            $('#account-doc').rules( "remove", "cpf" );
        }
    });
    $('input[name="type"]:checked').change();

};

$(document).ready(function (){
    new Meus_dados();
});