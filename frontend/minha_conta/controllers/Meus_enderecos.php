<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Meus_enderecos extends MY_Controller
{

    public function index()
    {
        if (!$this->st->client->get()){
            redirect('login');
        }

        $addresses = $this->st->addresses->get();

        $this->template
             ->set('title', 'Hotmusic - Meus Endereços')
             ->add_css('css/meus_enderecos')
             ->add_js('js/address', 'comum')
             ->add_js('js/meus_enderecos')
             ->set('addresses', $addresses)
             ->build('meus_enderecos');
    }

    public function form ($id = FALSE)
    {
        if ($this->input->is_ajax_request()){
            if (!$this->st->client->get()){
                $response = array(
                    'status'    => FALSE,
                    'class'     => 'error',
                    'title'     => 'Ocorreu um erro!',
                    'message'   => 'Você precisa estar logado para efetuar essa ação',
                    'redirect'  => site_url('login')
                );
            }else{
                $data = array('id' => $id);
                if($id){
                    $address = $this->st->addresses->get(array('id' => $id));
                    if (!$address){
                        $response = array(
                            'status'    => FALSE,
                            'class'     => 'error',
                            'title'     => 'Ocorreu um erro!',
                            'message'   => 'Você não tem premissão para acessar este endereço.'
                        );
                    }else{
                        $data['address'] = $address;
                    }
                }
                $response['view'] = $this->load->view('endereco_form', $data, TRUE);
            }
            $this->output->set_content_type('application/json')
                         ->set_output(json_encode($response));
        }else{
            show_404();
        }
    }

    public function add()
    {
        if (!$this->input->is_ajax_request())
            show_404();

        if (!$this->st->client->get()){
            $response = array(
                'status'    => FALSE,
                'class'     => 'error',
                'title'     => 'Ocorreu um erro!',
                'message'   => 'Você precisa estar logado para efetuar essa ação',
                'redirect'  => site_url('login')
            );
        }else{
            // Endereço
            $this->load->library('form_validation');
            $this->form_validation->set_rules('address[zipcode]', 'CEP', 'trim|required');
            $this->form_validation->set_rules('address[street]', 'Endereço', 'trim|required');
            $this->form_validation->set_rules('address[number]', 'Número', 'trim|required');
            $this->form_validation->set_rules('address[suburb]', 'Bairro', 'trim|required');
            $this->form_validation->set_rules('address[state]', 'Estado', 'trim|required');
            $this->form_validation->set_rules('address[city]', 'Cidade', 'trim|required');


            if ($this->form_validation->run()) {

                $data = $this->input->post('address');
                $origin = $this->input->post('origin');

                if (isset($data['id']))
                    $address = $this->st->addresses->edit($data);
                else
                    $address = $this->st->addresses->add($data);

                $view = $origin && $origin == 'cart' ? 'carrinho/endereco' : 'minha_conta/endereco';

                $response = array(
                    'status'=> TRUE,
                    'class' => 'success',
                    'title' => 'Sucesso!',
                    'reset' => TRUE,
                    'message' => (isset($data['id'])) ? 'Endereço editado com sucesso.' : 'Endereço adcionado com sucesso.',
                    'address' => array(
                        'id' => $address->id,
                        'zipcode' => $address->zipcode,
                        'label' => $address->label,
                        'address' => $address->street.', '.$address->number.', '.$address->complement.'<br>'.$address->city.' - '.$address->uf.'<br>CEP '.$address->zipcode
                    ),
                    'view' => $this->load->view($view, array('addresses' => array($address)), TRUE)
                );

            } else {
                $error = $this->form_validation->error_array();
                $response = array(
                    'status'=> FALSE,
                    'class' => 'error',
                    'title' => 'Ocorreu um erro!',
                    'message' => (count($error) > 1 ? 'Preencha os campos corretamente!' : array_shift($error)),
                    'fail' => $this->form_validation->error_array(TRUE)
                );
            }
        }

        $this->output->set_content_type('application/json')
                     ->set_output(json_encode($response));
    }

    public function edit()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('address[id]', 'ID do Endereço', 'trim|required');
        $this->add();
    }


    public function update()
    {
        if (!$this->input->is_ajax_request())
            show_404();

        $data = $this->input->post();

        //aqui atualizar o endereço de cobrança ou entrega

        $retorno = array(
            'status'=> TRUE,
            'classe' => 'success',
            'title' => 'Endereço atualizado!',
            'message' => $data['name'] == 'billing' ? 'Seu endereço de cobrança foi atualizado com sucesso.' : 'Seu principal endereço de entrega foi atualizado com sucesso.',
        );

        $this->output->set_content_type('application/json')
                     ->set_output(json_encode($retorno));
    }

    public function delete()
    {
        if (!$this->input->is_ajax_request())
            show_404();

        $id = $this->input->post('id');

        $this->st->addresses->delete($id);

        $retorno = array(
            'status'=> TRUE,
            'classe' => 'success',
            'title' => 'Endereço excluído!',
            'message' => 'O endereço foi excluído com sucesso.',
        );

        $this->output->set_content_type('application/json')
                     ->set_output(json_encode($retorno));
    }

}
