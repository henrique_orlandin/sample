<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Meus_dados extends MY_Controller
{

    public function index()
    {
        if (!$this->st->client->get()){
            redirect('login');
        }
        $client = $this->st->client->get(TRUE);
        $client->newsletter = $this->st->utils->get_newsletter($client->email);

        $route = array('minha-conta' => 'Minha Conta', 'Meus Dados');
        $this->template
             ->set('title', 'Hotmusic - Meus Dados')
             ->add_css('css/meus_dados')
             ->add_js('js/meus_dados')
             ->set('route', $route)
             ->set('client', $client)
             ->build('meus_dados');
    }

    public function edit()
    {
        if (!$this->input->is_ajax_request())
            show_404();

        if (!$this->st->client->get()){
            $response = array(
                'status'    => FALSE,
                'class'     => 'error',
                'title'     => 'Ocorreu um erro!',
                'message'   => 'Você precisa estar logado para efetuar essa ação',
                'redirect'  => site_url('login')
            );
        }else{
            // Validação
            $this->load->library('form_validation');
            $this->form_validation->set_rules('name', 'Nome', 'trim|required');
            $this->form_validation->set_rules('last_name', 'Sobrenome', 'trim|required');
            $this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email');
            $this->form_validation->set_rules('phone', 'Telefone', 'trim|required');
            if($this->input->post('type') == 'pf') {
                $this->form_validation->set_rules('gender', 'Sexo', 'trim|required');
                $this->form_validation->set_rules('birthdate', 'Data de Nascimento', 'trim|required');
                $this->form_validation->set_rules('cpf', 'CPF', 'trim|required|valid_cpf');
            } else
                $this->form_validation->set_rules('cpf', 'CNPJ', 'trim|required|valid_cnpj');

            if ($this->form_validation->run()) {

                $data = $this->input->post();
                if($data['type'] == 'pj') {
                    $data['gender'] = 'j';
                }

                $client = $this->st->client->edit($data);

                $response = array(
                    'status'=> TRUE,
                    'class' => 'success',
                    'title' => 'Sucesso!',
                    'message' => 'Seus dados foram atualizados com sucesso!'
                );
                if ($this->session->userdata('st_autocheckout')){
                    $response['redirect'] = site_url('carrinho/checkout');
                }

            } else {
                $error = $this->form_validation->error_array();
                $response = array(
                    'status'=> FALSE,
                    'class' => 'error',
                    'title' => 'Ocorreu um erro!',
                    'message' => (count($error) > 1 ? 'Preencha os campos corretamente!' : array_shift($error)),
                    'fail' => $this->form_validation->error_array(TRUE)
                );
            }
        }

        $this->output->set_content_type('application/json')
                     ->set_output(json_encode($response));
    }


}
