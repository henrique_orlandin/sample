<main id="order-detail" class="common-account">

    <?php $this->load->view('header',array('current_menu' => 'orders')) ?>

    <section class="content">

        <div class="content-top">
            <a href="<?php echo site_url('minha-conta/meus-pedidos'); ?>" class="common-back"><?php echo load_svg('back-arrow.svg'); ?></a>
            <h1 class="account-title">Pedido n&deg;<?php echo $order->reference; ?></h1>
        </div>

        <div class="overflow-scroll">
            <ul class="status-list">
                <li class="status-data <?php echo ($order->id_status === '0') ? 'current' : ''; ?>">
                    <div class="step">1</div>
                    <div class="desc">
                        <time class="date"><?php echo date("d/m/Y - H:i", strtotime($order->created)); ?>hrs</time>
                        <strong class="status">Aguardando Pagamento</strong>
                    </div>
                </li>

                <li class="status-data <?php echo ($order->id_status === '1') ? 'current' : ''; ?>">
                    <div class="step">2</div>
                    <div class="desc">
                        <?php if($order->id_status === '1'){ ?>
                            <strong class="status">Pagamento Aprovado</strong>
                        <?php }else if($order->id_status === '2'){ ?>
                            <strong class="status">Pedido Cancelado</strong>
                        <?php }else if($order->id_status === '3'){ ?>
                            <strong class="status">Pedido Extornado</strong>
                        <?php }else if ($order->id_status !== '0'){ ?>
                            <strong class="status">Aguardando Pagamento</strong>
                        <?php } else { ?>
                            <strong class="status">Pagamento Aprovado</strong>
                        <?php } ?>
                    </div>
                </li>

                <li class="status-data <?php echo ($order->id_status === '4') ? 'current' : ''; ?>">
                    <div class="step">3</div>
                    <div class="desc">
                        <strong class="status">Entrega</strong>
                    </div>
                </li>
                <li class="status-data <?php echo ($order->id_status === '5') ? 'current' : ''; ?>">
                    <div class="step">4</div>
                    <div class="desc">
                        <strong class="status">Finalizado</strong>
                    </div>
                </li>
            </ul>
        </div>

        <ul class="order-products-list">
            <?php foreach ($order->products as $key => $product){ ?>
            <li class="order-products-data">
                <?php
                $options = array(
                    'src'           => resize_url('image/resize?w=140&h=140&src='.$product->image),
                    'alt'           => $product->title,
                    'class'         => 'lazyload',
                    'data-viewport' => 1,
                    'tag'           => 'figure'
                );
                echo lazyload($options);
                ?>
                <div class="info">
                    <h2 class="title">
                        <?php echo $product->title; ?>
                        <small class="optional"><?php echo $product->variation_title; ?></small>
                    </h2>
                    <div class="price">
                        <?php if($product->original_price > $product->price){ ?>
                            <small class="old">De R$<?php echo mysql_decimal_to_number($product->original_price); ?></small>
                        <?php } ?>
                        <strong class="current">Por R$ <?php echo mysql_decimal_to_number($product->price); ?></strong>
                    </div>
                    <div class="right">
                        <div class="quantity">
                            <span class="label">Quant.</span>
                            <strong class="data"><?php echo $product->quantity; ?></strong>
                        </div>
                        <div class="total">
                            <span class="label">Subtotal</span>
                            <strong class="data">R$ <?php echo mysql_decimal_to_number($product->price * $product->quantity); ?></strong>
                        </div>
                    </div>
                </div>
            </li>
            <?php } ?>
        </ul>
        <div class="order-resume-list">
            <div class="order-resume-data">
                <div class="label">
                    Frete
                </div>
                <div class="data">
                    R$ <?php echo mysql_decimal_to_number($order->freight); ?>
                </div>
            </div>
            <div class="order-resume-data total">
                <div class="label">
                    Valor Total
                </div>
                <div class="data">
                    R$<?php echo mysql_decimal_to_number($order->price); ?>
                    <?php /*
                    <small class="payment">em 10x de 489,59 sem juros</small>
                    */ ?>
                </div>
            </div>
        </div>
    </section>
    <aside class="common-advantages">
        <?php $this->load->view('comum/advantages'); ?>
    </aside>
</main>