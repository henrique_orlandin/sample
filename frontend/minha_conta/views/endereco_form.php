<div class="content-top">
    <button class="common-back"><?php echo load_svg('back-arrow.svg'); ?></button>
    <h1 class="account-title"><?php echo $id ? 'Editar' : 'Adicionar novo'; ?> Endereço</h1>
</div>
<form action="<?php echo site_url('minha-conta/meus-enderecos/'.($id ? 'edit' : 'add')); ?>" method="POST" id="form-address" class="ajax-form common-form no-reset" novalidate>
    <?php if ($id){ ?>
    <input type="hidden" name="address[id]" value="<?php echo $id; ?>">
    <?php } ?>
    <div class="address-box">
        <div class="row">
            <div class="form-group">
                <input type="text" name="address[label]" id="address-label" value="<?php echo isset($address->label) ? $address->label : '' ?>" required placeholder="Descrição">
            </div>
            <div class="form-group">
                <input type="<?php echo $_is_mobile ? 'tel' : 'text'; ?>" name="address[zipcode]" id="address-zipcode" class="mask-zipcode" value="<?php echo isset($address->zipcode) ? $address->zipcode : '' ?>" required placeholder="CEP">
            </div>
        </div>
        <div class="row">
            <div class="form-group address-group">
                <input type="text" name="address[street]" id="address-street" value="<?php echo isset($address->street) ? $address->street : '' ?>" required placeholder="Endereço">
            </div>
            <div class="form-group number-group">
                <input type="number" name="address[number]" value="<?php echo isset($address->number) ? $address->number : '' ?>" id="address-number" placeholder="Número" required>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <input type="text" name="address[complement]" value="<?php echo isset($address->complement) ? $address->complement : '' ?>" id="address-complement" placeholder="Complemento">
            </div>
            <div class="form-group">
                <input type="text" name="address[suburb]" id="address-suburb" value="<?php echo isset($address->suburb) ? $address->suburb : '' ?>" required placeholder="Bairro">
            </div>
        </div>
        <div class="row">
            <div class="form-group relative">
                <?php $this->load->view('comum/preloader'); ?>
                <div class="select-wrapper">
                    <select name="address[state]" id="address-state" <?php echo isset($address->uf) ? 'data-value="'.$address->uf.'"' : '' ?> required>
                        <option value="">ESTADO</option>
                    </select>
                </div>
            </div>
            <div class="form-group relative">
                <?php $this->load->view('comum/preloader'); ?>
                <div class="select-wrapper">
                    <select name="address[city]" id="address-city" <?php echo isset($address->city) ? 'data-value="'.$address->city.'"' : '' ?> required class="search-select">
                        <option value="">CIDADE</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group final">
            <div class="checkbox-content">
                <div class="common-checkbox">
                    <input id="address-billing" name="address[billing]" type="checkbox" value="1" <?php echo isset($address->billing) && $address->billing == '1' ? 'checked="checked"' : '' ?>>
                    <div class="checkbox"></div>
                    <label for="address-billing">Meu Endereço de Cobrança</label>
                </div>
                <div class="common-checkbox">
                    <input id="address-default" name="address[default]" type="checkbox" value="1" <?php echo isset($address->default) && $address->default == '1' ? 'checked="checked"' : '' ?>>
                    <div class="checkbox"></div>
                    <label for="address-default">Meu Endereço de Entrega Padrão</label>
                </div>
            </div>
            <button type="submit" class="common-button">
                <?php $this->load->view('comum/preloader'); ?>
                <?php echo load_svg('save.svg'); ?>
                <span>Salvar</span>
            </button>
        </div>
    </div>
</form>