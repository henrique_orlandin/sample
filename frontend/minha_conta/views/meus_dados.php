<main id="account" class="common-account">
    <?php $this->load->view('header',array('current_menu' => 'user_data')) ?>
    <section class="content">

        <div class="content-top">
            <h1 class="account-title">Meus dados</h1>
        </div>

        <form action="<?php echo site_url('minha-conta/meus-dados/edit'); ?>" method="POST" id="form-account" class="ajax-form common-form no-reset" novalidate>
            <div class="row">
                <div class="form-group radio-group">
                    <div class="common-radio">
                        <input type="radio" name="type" id="radio-pf" value="pf" required <?php echo !isset($client->gender) || $client->gender != 'j' ? 'checked' : '' ?> />
                        <div class="radio"></div>
                        <label for="radio-pf">Pessoa Física</label>
                    </div>
                    <div class="common-radio">
                        <input type="radio" name="type" id="radio-pj" value="pj" required <?php echo isset($client->gender) && $client->gender == 'j' ? 'checked' : '' ?> />
                        <div class="radio"></div>
                        <label for="radio-pj">Pessoa jurídica</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <input type="text" name="name" id="account-name" value="<?php echo isset($client->name) ? $client->name : ''; ?>" required placeholder="Nome">
                </div>
                <div class="form-group">
                    <input type="text" name="last_name" id="account-last_name" value="<?php echo isset($client->last_name) ? $client->last_name : ''; ?>" required placeholder="Sobrenome">
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <input type="email" name="email" id="account-email" value="<?php echo isset($client->email) ? $client->email : ''; ?>" required placeholder="E-mail">
                </div>
                <div class="form-group">
                    <input type="<?php echo $_is_mobile ? 'tel' : 'text'; ?>" name="cpf" id="account-doc" value="<?php echo isset($client->cpf) ? $client->cpf : ''; ?>" required placeholder="CPF">
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <input type="<?php echo $_is_mobile ? 'tel' : 'text'; ?>" name="phone" id="account-phone" class="mask-phone" value="<?php echo isset($client->phone) ? $client->phone : ''; ?>" required placeholder="Telefone">
                </div>
                <div class="form-group">
                    <input type="<?php echo $_is_mobile ? 'tel' : 'text'; ?>" name="phone_mobile" id="account-phone2" class="mask-phone" value="<?php echo isset($client->phone_mobile) ? $client->phone_mobile : ''; ?>" placeholder="Celular">
                </div>
            </div>
            <div class="row pf-data">
                <div class="form-group">
                    <div class="select-wrapper">
                        <select name="gender" id="account-gender" placeholder="SEXO" required>
                            <option value="">SEXO</option>
                            <option value="m" <?php echo isset($client->gender) && $client->gender == 'm' ? 'selected="selected"' : ''; ?>>Masculino</option>
                            <option value="f" <?php echo isset($client->gender) && $client->gender == 'f' ? 'selected="selected"' : ''; ?>>Feminino</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <input type="text" name="birthdate" id="account-birthdate" class="mask-date" value="<?php echo isset($client->birthdate) ? $client->birthdate : ''; ?>" placeholder="Data de Nascimento" required>
                </div>
            </div>
            <div class="row">
                <div class="form-group terms">
                    <div class="common-checkbox">
                        <input type="checkbox" name="newsletter" id="check-news" value="1" <?php echo isset($client->newsletter) && !empty($client->newsletter) && $client->newsletter->status == '1' ? 'checked="checked"' : ''; ?> />
                        <div class="checkbox"></div>
                        <label for="check-news">Desejo receber promoções e ofertas da Hotmusic</label>
                    </div>
                    <button type="submit" class="common-button">
                        <?php $this->load->view('comum/preloader'); ?>
                        <?php echo load_svg('save.svg'); ?>
                        <span>Salvar</span>
                    </button>
                </div>
            </div>
        </form>
    </section>
    <aside class="common-advantages">
        <?php $this->load->view('comum/advantages'); ?>
    </aside>
</main>