<?php foreach ($addresses as $address){ ?>
<div class="address-content" data-id="<?php echo $address->id; ?>">
    <div class="address-info">
        <h4 class="title"><?php echo $address->label; ?></h4>
        <div class="common-text">
            <p>
                <?php echo $address->street.', '.$address->number.($address->complement ? ', '.$address->complement : '').
                ', Bairro: '.$address->suburb.' - '.$address->zipcode.', '.$address->city.' - '.$address->uf; ?>
            </p>
        </div>
        <div class="common-checkbox">
            <input id="charge-address-<?php echo $address->id; ?>" name="billing" type="radio" value="<?php echo $address->id; ?>" <?php echo ($address->billing=='1') ? 'checked="checked"' : ''; ?>>
            <div class="checkbox"></div>
            <label for="charge-address-<?php echo $address->id; ?>">Endereço de cobrança</label>
        </div>
        <div class="common-checkbox">
            <input id="default-address-<?php echo $address->id; ?>" name="default" type="radio" value="<?php echo $address->id; ?>" <?php echo ($address->default=='1') ? 'checked="checked"' : ''; ?>>
            <div class="checkbox"></div>
            <label for="default-address-<?php echo $address->id; ?>">Endereço de entrega padrão</label>
        </div>
    </div>
    <div class="address-actions">
        <button type="button" class="delete-address">
            <?php echo load_svg('trash.svg'); ?>
        </button>
        <button class="common-button change-address">
            <?php echo load_svg('edit.svg'); ?>
            <span>Editar</span>
        </button>
    </div>
    <div class="address-loader">
        <?php $this->load->view('comum/preloader'); ?>
    </div>
</div>
<?php } ?>