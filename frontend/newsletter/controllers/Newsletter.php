<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH."libraries/activecampaign/ActiveCampaign.class.php");

class Newsletter extends MY_Controller
{

    public function send()
    {
        if (!$this->input->is_ajax_request())
            show_404();

        // Validação
        $this->load->library('form_validation');

        $this->form_validation->set_rules('email', 'E-mail', 'required|valid_email');

        if ($this->form_validation->run())
        {
            $newsletter = array(
                'name'   => $this->input->post('name'),
                'email'  => $this->input->post('email'),
                'status' => 1
            );

            if (!$this->input->post('name') && $client = $this->st->client->get()){
                if ($client->email == $this->input->post('email')){
                    $newsletter['name'] = $client->name;
                }
            }

            // Active Campaign
            $ac = new ActiveCampaign(ACTIVECAMPAIGN_URL, ACTIVECAMPAIGN_API_KEY);

            if ((int)$ac->credentials_test()) {

                $list_id = ACTIVECAMPAIGN_LIST_ID;
                $name = explode(' ', $newsletter['name']);
                $contact = array(
                    "email"              => $newsletter['email'],
                    "first_name"         => array_shift($name),
                    "last_name"          => !empty($name) ? array_pop($name) : '',
                    "p[{$list_id}]"      => $list_id,
                    "status[{$list_id}]" => 1,
                );
                $contact_sync = $ac->api("contact/sync", $contact);
            }
            // Fim Active Campaign


            if ($this->st->utils->add_newsletter($newsletter)) {
                $retorno = array('status'=> TRUE, 'class' => 'success', 'title' => 'Sucesso!', 'message' => 'Seu cadastrado foi efetuado com sucesso!', 'reset' => TRUE);
            }else{
                $retorno = array('status'=> FALSE, 'class' => 'alert', 'title' => 'Atenção!', 'message' => 'Este email já está cadastrado!');
            }

        } else {
            $error = $this->form_validation->error_array();
            $retorno = array('status'=> FALSE, 'class' => 'error', 'title' => 'Ocorreu um erro!', 'message' => (count($error) > 1 ? 'Preencha os campos corretamente!' : array_shift($error)), 'fail' => $this->form_validation->error_array(TRUE));
        }

        $this->output->set_content_type('application/json')
                     ->set_output(json_encode($retorno));
    }
}
