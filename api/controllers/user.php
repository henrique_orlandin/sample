<?php (defined('BASEPATH')) or exit('No direct script access allowed');

require_once(__DIR__ . '/api_controller.php');

class User extends Api_controller
{

    public function __construct($isRun = FALSE)
    {
        $this->require_auth = FALSE;
        parent::__construct(TRUE);
        $this->load->model('user_m');
    }

    /**
     * User login
     * @return void
     * @author Henrique Orlandin <henrique.orlandin@gmail.com>
     */
    public function login ()
    {
        try {
            $data = $this->input->get();
            $user = $this->user_m->auth($data);
            $this->json($user);
        } catch(Exception $e) {
            $this->error($e->getMessage());
        }
    }

}
