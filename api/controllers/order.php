<?php (defined('BASEPATH')) or exit('No direct script access allowed');

require_once(__DIR__ . '/api_controller.php');

class Order extends Api_controller
{

    public function __construct($isRun = FALSE)
    {
        $this->require_auth = TRUE;
        parent::__construct(TRUE);
        $this->load->model(PATH_TO_MODEL.'app_produtos/models/app_produtos_m');
        $this->load->model('order_m');
    }

    /**
     * Order list
     * @param int $id
     * @return void
     * @author Henrique Orlandin <henrique@grupoezoom.com.br>
     */
    public function get ($id = FALSE)
    {
        try {
            $pg = $this->input->get('pg') ? $this->input->get('pg') : 1;
            $start = $pg - 1;
            $offset = $start * 10;

            $data = $this->order_m->get(array(
                'limit' => 10,
                'offset' => $offset,
                'id' => $id
            ));
            $this->json($data);
        } catch(Exception $e) {
            $this->error($e->getMessage());
        }
    }

    /**
     * check order price
     * @return void
     * @author Henrique Orlandin <henrique@grupoezoom.com.br>
     */
    public function validate ()
    {
        try {
            $post = $this->clear($this->input->post());
            $first = current($post['products']);
            if(!isset($first['id']))
                $this->error('Your list is empty.');

            $status = $this->check_price($post);
            $this->json($status);
        } catch(Exception $e) {
            $this->error($e->getMessage());
        }
    }

    /**
     * order insert
     * @return void
     * @author Henrique Orlandin <henrique@grupoezoom.com.br>
     */
    public function register ()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('id_client', T_('Client'), 'trim|required');
        $this->form_validation->set_rules('id_payment', T_('Payment'), 'trim|required');
        $this->form_validation->set_rules('id_loading', T_('Load'), 'trim|required');
        $this->form_validation->set_rules('products', T_('Products'), 'required');

        if ($this->form_validation->run() === TRUE) {

            try {
                $post = $this->clear($this->input->post());
                $first = current($post['products']);

                $this->load->model('loading_m');
                $loading = $this->loading_m->get(array('id' => $post['id_loading']));
                if(empty($loading))
                    $this->error('Load not found.');

                if(!isset($first['id']))
                    $this->error('Your list is empty.');

                if(!$this->check_price($post))
                    $this->error('Incorrect order price.');

                $insert = $this->order_m->insert($post);

                if($insert)
                    $this->loading_m->update($post['id_loading']);


                $this->json($insert);

            } catch(Exception $e) {
                $this->error($e->getMessage());
            }

        } else {
            $errors = array_values($this->form_validation->error_array());
            $this->error($errors[0]);
        }
    }

    /**
     * Order update
     * @param int $id
     * @return void
     * @author Henrique Orlandin <henrique@grupoezoom.com.br>
     */
    public function update ($id)
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('id_client', T_('Cliente'), 'trim|required');
        $this->form_validation->set_rules('id_payment', T_('Pagamento'), 'trim|required');
        $this->form_validation->set_rules('products', T_('Produtos'), 'required');

        if ($this->form_validation->run() === TRUE) {

            try {
                $post = $this->clear($this->input->post());
                $first = current($post['products']);

                $this->load->model('loading_m');
                $loading = $this->loading_m->get(array('id' => $post['id_loading']));
                if(empty($loading))
                    $this->error('Load not found.');

                if(!isset($first['id']))
                    $this->error('Your list is empty.');

                if(!$this->check_price($post))
                    $this->error('Incorrect order price.');

                $update = $this->order_m->update($id, $post);

                if($update) {
                    $this->load->model('loading_m');
                    $this->loading_m->update($post['id_loading']);
                }

                $this->json($update);

            } catch(Exception $e) {
                $this->error($e->getMessage());
            }

        } else {
            $errors = array_values($this->form_validation->error_array());
            $this->error($errors[0]);
        }
    }

    /**
     * Check the minimum price
     * @param array $post
     * @return void
     * @author Henrique Orlandin <henrique@grupoezoom.com.br>
     */
    private function check_price ($post)
    {
        $ids = array();
        $current_value = 0;
        $sum_min = 0;
        foreach ($post['products'] as $key => $value) {
            $current_value += $value['quantity'] * $value['unit_price'];
            $min = $this->app_produtos_m->get(array('sum' => $value['id'], 'where' => array('status' => TRUE)));
            $sum_min += $min * $value['quantity'];
        }
        return $current_value >= $sum_min ? TRUE : FALSE;
    }

    /**
     * Clear price format
     * @param array $post
     * @return void
     * @author Henrique Orlandin <henrique@grupoezoom.com.br>
     */
    private function clear ($post)
    {
        foreach ($post['products'] as $key => $value) {
            $post['products'][$key]['unit_price'] = trim(str_replace('R$','',str_replace(',','.',str_replace('.','',$value['unit_price']))));
        }
        return $post;
    }

    /**
     * Update orders order
     * @return void
     * @author Henrique Orlandin <henrique@grupoezoom.com.br>
     */
    public function sort ()
    {
        try {
            $data = $this->input->post();
            if(!isset($data['item']) || empty($data['item']))
                $this->error('No item selected!');

            $status = $this->order_m->sort($data['item']);
            if($status) {
                $this->json('Successfully updated.');
            } else {
                $this->error('Order error.');
            }
        } catch(Exception $e) {
            $this->error($e->getMessage());
        }
    }

}
