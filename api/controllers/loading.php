<?php (defined('BASEPATH')) or exit('No direct script access allowed');

require_once(__DIR__ . '/api_controller.php');

class Loading extends Api_controller
{

    public function __construct($isRun = FALSE)
    {
        $this->require_auth = TRUE;
        parent::__construct(TRUE);
        $this->load->model('loading_m');
    }

    /**
     * Load list
     * @param int $id
     * @return void
     * @author Henrique Orlandin <henrique@grupoezoom.com.br>
     */
    public function get ($id = FALSE)
    {
        try {
            $pg = $this->input->get('pg') ? $this->input->get('pg') : 1;
            $start = $pg - 1;
            $offset = $start * 10;

            $data = $this->loading_m->get(array(
                'limit' => 10,
                'offset' => $offset,
                'id' => $id
            ));
            $this->json($data);
        } catch(Exception $e) {
            $this->error($e->getMessage());
        }
    }


    /**
     * Loads per product
     * @param int $id
     * @return void
     * @author Henrique Orlandin <henrique.orlandin@gmail.com>
     */
    public function products_view ($id)
    {
        try {
            $data = $this->loading_m->get(array('id' => $id, 'product_view' => TRUE));
            $this->json($data);
        } catch(Exception $e) {
            $this->error($e->getMessage());
        }
    }


    /**
     * Status Update
     * @param [type] $id
     * @return void
     * @author Henrique Orlandin <henrique.orlandin@gmail.com>
     */
    public function change_status ($id)
    {
        try {
            $post = $this->input->post();
            $response = $this->loading_m->change_status($id, $post['status']);
            $this->json($response);
        } catch(Exception $e) {
            $this->error($e->getMessage());
        }
    }

    /**
     * Load Insert
     * @return void
     * @author Henrique Orlandin <henrique.orlandin@gmail.com>
     */
    public function register ()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('date', T_('Date'), 'trim|required|valid_date');

        if ($this->form_validation->run() === TRUE) {
            $data = $this->input->post();
            try {
                $return = $this->loading_m->insert($data);
                $this->json($return);
            } catch(Exception $e) {
                $this->error($e->getMessage());
            }
        } else {
            $errors = array_values($this->form_validation->error_array());
            $this->error($errors[0]);
        }
    }

}
