<?php (defined('BASEPATH')) or exit('No direct script access allowed');

require_once(__DIR__ . '/api_controller.php');

class Product extends Api_controller
{

    public function __construct($isRun = FALSE)
    {
        $this->require_auth = TRUE;
        parent::__construct(TRUE);
        $this->load->model(PATH_TO_MODEL.'app_produtos/models/app_produtos_m');
    }

    /**
     * Product List
     * @param boolean $id
     * @return void
     * @author Henrique Orlandin <henrique.orlandin@gmail.com>
     */
    public function get ($id = FALSE)
    {
        try {

            $get = $this->input->get();
            if(isset($get['search']))
                $search = array('title' => $get['search']);
            else
                $search = FALSE;

            $pg = isset($get['pg']) ? $get['pg'] : 1;
            $start = $pg - 1;
            $offset = $start * 10;

            $data = $this->app_produtos_m->get(array(
                'limit' => 10,
                'offset' => $offset,
                'search' => $search,
                'id' => $id,
                'where' => array('status' => TRUE)
            ));
            $this->json($data);
        } catch(Exception $e) {
            $this->error($e->getMessage());
        }
    }

}
