<?php (defined('BASEPATH')) or exit('No direct script access allowed');

require_once(__DIR__ . '/api_controller.php');

class Utils extends Api_controller
{

    public function __construct($isRun = FALSE)
    {
        $this->require_auth = FALSE;
        parent::__construct(TRUE);
        $this->load->model('comum/comum_m');
    }

    /**
     * State list
     * @return void
     * @author Henrique Orlandin <henrique.orlandin@gmail.com>
     */
    public function states ()
    {
        $response = array(
            'status' => TRUE,
            'data'   => $this->comum_m->get_states()
        );

        $this->json($response);
    }

    /**
     * City list
     * @return void
     * @author Henrique Orlandin <henrique.orlandin@gmail.com>
     */
    public function cities ()
    {
        $state = $this->input->get('state');
        if (!$state){
            $this->error(T_('State is required'));
        }

        $cities = $this->comum_m->get_cities($state);

        $this->unset_fields($cities, array('id_solar_radiation'));

        $response = array(
            'status' => TRUE,
            'data'   => $cities
        );

        $this->json($response, 200, FALSE);
    }

}
