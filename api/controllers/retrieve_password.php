<?php (defined('BASEPATH')) or exit('No direct script access allowed');

require_once(__DIR__ . '/api_controller.php');

class Retrieve_password extends Api_controller
{

    public function __construct($isRun = FALSE)
    {
        $this->require_auth = FALSE;
        parent::__construct(TRUE);
        $this->load->model('user_m');
    }

    /**
     * E-mail to retrieve password
     * @return void
     * @author Henrique Orlandin <henrique.orlandin@gmail.com>
     */
    public function index ()
    {
        try {
            $data = $this->input->get();
            $user = $this->user_m->retrieve_password($data);
            $this->json($user);
        } catch(Exception $e) {
            $this->error($e->getMessage());
        }
    }

    /**
     * Check retrieve hash validity
     * @return void
     * @author Henrique Orlandin <henrique.orlandin@gmail.com>
     */
    public function check_hash ()
    {
        try {
            $data = $this->input->get();
            $hash = $this->user_m->retrieve_check_hash($data);
            $this->json($hash);
        } catch(Exception $e) {
            $this->error($e->getMessage());
        }
    }

    /**
     * New password
     * @return void
     * @author Henrique Orlandin <henrique.orlandin@gmail.com>
     */
    public function change_password ()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('hash', T_('Hash'), 'trim|required');
        $this->form_validation->set_rules('password', T_('Password'), 'trim|required|matches[repeat_password]|min_length[5]|password_strenght');
        $this->form_validation->set_rules('repeat_password', T_('Confirm Password'), 'trim|required');

        if ($this->form_validation->run() === TRUE) {
            try {
                $data = $this->input->post();
                $return = $this->user_m->retrieve_check_hash($data);

                if ($return['status']){

                    $user = $return['data'];
                    $return = $this->user_m->update_password(array(
                        'user'              => $user,
                        'password'          => $data['password'],
                        'repeat_password'   => $data['repeat_password']
                    ));

                    if ($return){
                        $return = $this->user_m->auth(array(
                            'email'     => $user->email,
                            'password'  => $data['password']
                        ));
                    }else{
                        return $this->error();
                    }
                }
                $this->json($return);
            } catch(Exception $e) {
                $this->error($e->getMessage());
            }
        } else {
            $errors = array_values($this->form_validation->error_array());
            $this->error($errors[0]);
        }

    }
}
