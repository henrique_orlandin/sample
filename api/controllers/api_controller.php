<?php (defined('BASEPATH')) or exit('No direct script access allowed');

class Api_controller extends MY_Controller
{

    protected $auth;
    protected $require_auth = TRUE;
    private $token = 'FNKRT8250';
    private $version = '1';
    private $session_id = null;

    public function __construct($isRun = FALSE)
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization, api-token, api-version, session-id');

        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS'){
            die();
        }

        register_shutdown_function(array($this, 'handleShutdown'));

        parent::__construct(TRUE);

        $this->load->model('api_m');
        $this->validate_connection();

    }

    /**
     * connection validation
     * @return void
     * @author Henrique Orlandin <henrique.orlandin@gmail.com>
     */
    protected function validate_connection ()
    {

        $this->auth = $this->api_m->auth;

        if (!$this->auth->token || $this->auth->token != $this->token || ($this->require_auth && !$this->session_id && !$this->auth->user)){
            $this->json(array(
                'status' => FALSE,
                'data' => 'Unauthorized request.'
            ), 401);
        }

    }

    /**
     * API return pathern
     * @param array $output
     * @param integer $code
     * @param boolean $auto_unset
     * @return string
     * @author Henrique Orlandin <henrique.orlandin@gmail.com>
     */
    protected function json ($output, $code = 200, $auto_unset = TRUE)
    {

        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');

        $this->set_status_header($code);
        if(!(is_array($output) && isset($output['status']))) {
            $output = array(
                'status'    => TRUE,
                'data'      => $output
            );
        }
        if (is_array($output['data']) || is_object($output['data'])) {
            $output['data'] = (array) $output['data'];

            if ($auto_unset) {
                $this->unset_fields($output['data']);
            }
        }
        die(json_encode($output));

    }

    /**
     * Special case to deal with Fatal errors and the like.
     */
    public function handleShutdown()
    {
        $error = error_get_last();
        $fatal_errors = array(
            E_ERROR,
            E_PARSE,
            E_CORE_ERROR,
            E_CORE_WARNING,
            E_COMPILE_ERROR,
            E_COMPILE_WARNING
        );
        if (!empty($error) && in_array($error['type'], $fatal_errors)) {
            $message = $error['message'].' (Line: '.$error['line'].' - File: '.$error['file'].')';
            $this->error($message, 500);
        }
    }

    /**
     * Error message
     * @param boolean $message
     * @param integer $code
     * @return void
     * @author Henrique Orlandin <henrique.orlandin@gmail.com>
     */
    protected function error ($message = FALSE, $code = 200)
    {
        $json = array(
            'status'    => FALSE,
            'data'      => $message ? $message : 'An error has occurred. Try again later.'
        );
        if ($code != 200) {
            $json['data']   = 'An error has occurred. Try again later.';
            $json['error']  = $message;
        }
        $this->json($json, $code);
    }

    /**
     * remove undesirable fields from returned data
     * @param [type] $item
     * @param array $fields
     * @return void
     * @author Henrique Orlandin <henrique.orlandin@gmail.com>
     */
    public function unset_fields (&$item, $fields = array())
    {
        $removeColumns = array('password', 'languages', 'id_company', 'id_language', 'order_by', 'created', 'updated', 'deleted');
        $removeColumns = array_merge($removeColumns, $fields);

        foreach ($removeColumns as $key => $field) {
            if (is_array($item)) {
                unset($item[$field]);
                foreach ($item as $child) {
                    $this->unset_fields($child, $removeColumns);
                }
            } else if (is_object($item)) {
                unset($item->{$field});
            }
        }
    }

    private function set_status_header($code = 200, $text = '')
    {

        if (empty($text)){

            is_int($code) OR $code = (int) $code;
            $stati = array(
                100 => 'Continue',
                101 => 'Switching Protocols',

                200 => 'OK',
                201 => 'Created',
                202 => 'Accepted',
                203 => 'Non-Authoritative Information',
                204 => 'No Content',
                205 => 'Reset Content',
                206 => 'Partial Content',

                300 => 'Multiple Choices',
                301 => 'Moved Permanently',
                302 => 'Found',
                303 => 'See Other',
                304 => 'Not Modified',
                305 => 'Use Proxy',
                307 => 'Temporary Redirect',

                400 => 'Bad Request',
                401 => 'Unauthorized',
                402 => 'Payment Required',
                403 => 'Forbidden',
                404 => 'Not Found',
                405 => 'Method Not Allowed',
                406 => 'Not Acceptable',
                407 => 'Proxy Authentication Required',
                408 => 'Request Timeout',
                409 => 'Conflict',
                410 => 'Gone',
                411 => 'Length Required',
                412 => 'Precondition Failed',
                413 => 'Request Entity Too Large',
                414 => 'Request-URI Too Long',
                415 => 'Unsupported Media Type',
                416 => 'Requested Range Not Satisfiable',
                417 => 'Expectation Failed',
                422 => 'Unprocessable Entity',

                500 => 'Internal Server Error',
                501 => 'Not Implemented',
                502 => 'Bad Gateway',
                503 => 'Service Unavailable',
                504 => 'Gateway Timeout',
                505 => 'HTTP Version Not Supported'
            );

            if (isset($stati[$code]))
            {
                $text = $stati[$code];
            }
        }

        if (strpos(PHP_SAPI, 'cgi') === 0)
        {
            header('Status: '.$code.' '.$text, TRUE);
        }
        else
        {
            $server_protocol = $this->input->server('SERVER_PROTOCOL') ? $this->input->server('SERVER_PROTOCOL') : 'HTTP/1.1';
            header($server_protocol.' '.$code.' '.$text, TRUE, $code);
        }
    }

}
