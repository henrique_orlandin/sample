<?php (defined('BASEPATH')) or exit('No direct script access allowed');

require_once(__DIR__ . '/api_m.php');

/**
 * Model
 *
 * @subpackage API
 * @category Model
 * @author Henrique Orlandin
 * @copyright 2018 Henri
 */
class User_m extends Api_m
{

    public $table = 'app_user';
    public $table_type = 'app_user_type';

    public function __construct()
    {
        parent::__construct();
    }

    public function get($params = array())
    {
        $options = array(
            'count'     => FALSE,
            'id'        => FALSE,
            'email'     => FALSE,
            'where'     => FALSE,
            'complete'  => FALSE
        );
        $params = array_merge($options, $params);

        if ($params['count'])
            $this->db->select('COUNT(DISTINCT '.$this->table.'.id) AS count');
        else {
            $this->db->select($this->table.'.*')
                     ->select($this->table_type.'.title as type');
        }

        $this->db->from($this->table)
                 ->join($this->table_type, $this->table_type.'.id = '.$this->table.'.id_type', 'INNER');

        if ($params['id']){
            $this->db->where($this->table.'.id', $params['id']);
        }
        if ($params['email']){
            $this->db->where($this->table.'.email', $params['email']);
        }

        if ($params['where'] !== FALSE){
            if (is_array($params['where']))
                $this->db->where($params['where']);
            else
                $this->db->where($params['where'], FALSE, FALSE);
        }

        $this->db->where($this->table.'.status', TRUE)
                 ->where($this->table.'.deleted IS NULL', FALSE, FALSE);

        $query = $this->db->get();
        $toReturn = array();

        if ($params['count']){
            $data = $query->row();
            $toReturn = (int) $data->count;
        } else if ($params['id'] || $params['email']){

            $data = $query->row();
            if (!$data)
                return FALSE;

            $toReturn = $data;

        } else {
            $toReturn = $query->result();
        }

        return $toReturn;
    }

    public function auth ($params = array())
    {
        $options = array(
            'email'     => null,
            'password'  => null,
        );
        $params = (object) array_merge($options, $params);

        $this->load->library('PasswordHash');

        $result = $this->get(array('email' => $params->email));

        if ($result) {
            if ($this->passwordhash->CheckPassword($params->password, $result->password)) {
                return $this->auth_session($result);
            } else {
                throw new Exception("E-mail ou senha inválidos.", 1);
            }
        } else {
            throw new Exception("E-mail ou senha inválidos.", 1);
        }
    }

    public function update_password ($params = array())
    {
        $options = array(
            'user'              => null,
            'password'          => null,
            'repeat_password'   => null
        );
        $params = (object) array_merge($options, $params);

        $user = $params->user;

        if (empty($user) || !$user){
            throw new Exception('O usuário não foi encontrado.', 1);
        }

        $this->load->library('PasswordHash');
        $new_password = $this->passwordhash->HashPassword($params->password);

        $this->db->trans_start();

        $this->db->where('id', $user->id)
                 ->update($this->table, array(
                    'password'       => $new_password,
                    'retrieve_hash'  => null
                ));

        $this->db->trans_complete();

        return $this->db->trans_status();
    }

    public function retrieve_password ($params = array())
    {
        $options = array(
            'email' => null
        );
        $params = (object) array_merge($options, $params);

        $user = $this->get(array('email' => $params->email));

        if (!empty($user) && $user){

            $this->load->helper('email');

            $company = (object)$_SESSION['company'];

            $md5 = md5(uniqid(rand(),TRUE));
            $hash = substr($md5, 0, 16).'::'.strtotime(date('Y-m-d H:i:s'). ' + 1 days').'::'.substr($md5, 16);

            $this->db->where('id', $user->id)
                     ->update($this->table, array('retrieve_hash' => $hash));

            $to = (ENVIRONMENT == 'production') ? array('to' => $user->email) : array('to' => array('henrique@grupoezoom.com.br'));

            $body = '<b>' . T_('Olá') . ', ' . $user->name . '</b><br /><br />';
            $body .= T_('We received your request.').'<br />';
            $body .= T_('Clink on the link below to reset your password:').'<br />';
            $body .= '<a href="' . (APP_URL . 'recuperar-senha/' . $hash) . '" target="_blank">' . (APP_URL . 'recuperar-senha/' . $hash) . '</a><br /><br />';
            $body .= T_('If you did not request a new password, contact us as soon as possible.').'<br /><br />';
            $body .= '<i><small>'.T_('This is an automatic message, please do not replay.').'</small></i>';

            $email = enviar_email($to, (T_('Recuperação de senha') . ' - ' . $company->name), $body);

            if ($email){
                $return = array(
                    'status' => TRUE,
                    'data' => 'You will receive an e-mail with a link to change your password.'
                );
                if (ENVIRONMENT == 'development'){
                    $return['hash'] = $hash;
                }
                return $return;
            } else {
                throw new Exception(T_('An error ocurred. Try again later'), 1);
            }

        } else {
            throw new Exception(T_('E-mail not found.'), 1);
        }
    }

    public function retrieve_check_hash ($params = array())
    {
        $options = array(
            'hash' => null
        );
        $params = (object) array_merge($options, $params);

        $invalidMessage = array(
            'status' => FALSE,
            'data' => 'This link is not valid.'
        );

        if (!$params->hash){
            return $invalidMessage;
        }

        $hash = $this->get(array(
            'where' => array('retrieve_hash' => $params->hash)
        ));

        if (!empty($hash) && $hash){
            $hash = current($hash);
            // Confere se hash esta expirado
            $parts = explode('::', $hash->retrieve_hash);
            if (count($parts) == 3){
                if (strtotime(date('Y-m-d H:i:s')) <= (int) $parts[1]){
                    return array(
                        'status' => TRUE,
                        'data' => $hash
                    );
                }
            }
        }

        return $invalidMessage;
    }

    private function auth_session ($user)
    {
        $session_id = '';
        while (strlen($session_id) < 32){
            $session_id .= mt_rand(0, mt_getrandmax());
        }
        $session_id .= md5($user->email);
        $session_id = md5(uniqid($session_id, TRUE));

        $user->session_id = $session_id;

        $this->db->insert('app_session', array(
            'session_id'    => $session_id,
            'ip_address'    => $this->input->ip_address(),
            'user_agent'    => 'Device',
            'user_data'     => serialize($user)
        ));

        unset($user->id_type, $user->retrieve_hash);

        return $user;
    }

}
