<?php (defined('BASEPATH')) or exit('No direct script access allowed');

require_once(__DIR__ . '/api_m.php');

/**
 * Model
 *
 * @subpackage API
 * @category Model
 * @author Henrique Orlandin
 * @copyright 2018 Henri
 */
class Order_m extends Api_m
{

    public $table = 'app_order';

    public function __construct()
    {
        parent::__construct();
    }

    public function get($params = array())
    {
        $options = array(
            'count' => FALSE,
            'id'    => FALSE,
            'where' => FALSE,
            'limit' => FALSE,
            'offset' => FALSE,
        );
        $params = array_merge($options, $params);

        if ($params['count'])
            $this->db->select('COUNT(DISTINCT '.$this->table.'.id) AS count');
        else {
            $this->db->select($this->table.'.*, c.name as client')
                     ->order_by('order_by');

            if ($params['limit'] !== FALSE && $params['offset'] === FALSE)
                $this->db->limit($params['limit']);
            elseif ($params['limit'] !== FALSE)
                $this->db->limit($params['limit'], $params['offset']);
        }

        $this->db->from($this->table)
                 ->join('app_client as c', 'c.id = '.$this->table.'.id_client', 'INNER')
                 ->join('app_loading as l', 'l.id = '.$this->table.'.id_loading', 'INNER')
                 ->where('l.id_user', $this->auth->user->id);

        if ($params['id']) {
            $this->db->where($this->table.'.id', $params['id']);
        }

        if ($params['where'] !== FALSE) {
            if (is_array($params['where']))
                $this->db->where($params['where']);
            else
                $this->db->where($params['where'], FALSE, FALSE);
        }

        $query = $this->db->get();
        $toReturn = array();

        if ($params['count']){
            $data = $query->row();
            $toReturn = (int) $data->count;
        } else if ($params['id']){

            $data = $query->row();
            if (!$data)
                return FALSE;

            $this->db->select('op.*, pd.title')
                     ->from('app_product as p')
                     ->join('app_product_description as pd', 'pd.id_product = p.id', 'INNER')
                     ->join('app_order_product as op', 'op.id_product = p.id', 'INNER')
                     ->where('op.id_order', $data->id)
                     ->where('pd.id_language',$this->current_lang);
            $query = $this->db->get();
            $data->products = $query->result();

            $toReturn = $data;

        } else {
            $toReturn = $query->result();
        }

        return $toReturn;
    }

    public function insert($data)
    {
        $this->db->trans_start();

        $order_by = $this->get(array(
            'count' => TRUE,
            'where' => array('id_loading' => $data['id_loading'])
        ));

        $insert =  array(
            'order_by'      => ($order_by + 1),
            'id_loading'    => $data['id_loading'],
            'id_client'     => $data['id_client'],
            'id_payment'    => $data['id_payment'],
        );

        $this->db->insert($this->table, $insert);

        $id = $this->db->insert_id();

        $order_weight = 0;
        $order_quantity = 0;
        $order_price = 0;
        $insert_products = array();
        foreach ($data['products'] as $key => $value) {
            $this->db->select('weight')
                     ->from('app_product')
                     ->where('id', $value['id']);
            $query = $this->db->get();
            $data = $query->row();

            $order_weight += $value['quantity'] * $data->weight;
            $order_quantity += $value['quantity'];
            $order_price += $value['quantity'] * $value['unit_price'];
            $insert_products[] = array(
                'id_order'      => $id,
                'id_product'    => $value['id'],
                'quantity'      => $value['quantity'],
                'unit_price'    => $value['unit_price'],
                'total_price'   => number_format($value['quantity'] * $value['unit_price'],2,'.',''),
            );
        }

        if(!empty($insert_products)) {
            $this->db->insert_batch('app_order_product', $insert_products);
        }

        $update = array(
            'weight'    => number_format($order_weight, 2,'.',''),
            'quantity'  => number_format($order_quantity, 2,'.',''),
            'price'     => number_format($order_price, 2,'.',''),
        );

        $this->db->where('id',$id)->update($this->table, $update);

        $this->db->trans_complete();

        return $this->db->trans_status() ? $this->get(array('id' => $id)) : FALSE;
    }

    public function update($id, $data)
    {
        $this->db->trans_start();
        $current = $this->get(array('id' => $id));

        if (!$id || !$current)
            return false;

        $order_weight = 0;
        $order_quantity = 0;
        $order_price = 0;
        $insert_products = array();
        foreach ($data['products'] as $key => $value) {
            $this->db->select('weight')
                     ->from('app_product')
                     ->where('id', $value['id']);
            $query = $this->db->get();
            $product = $query->row();

            $order_weight += $value['quantity'] * $product->weight;
            $order_quantity += $value['quantity'];
            $order_price += $value['quantity'] * $value['unit_price'];

            $insert_products[] = array(
                'id_order'      => $id,
                'id_product'    => $value['id'],
                'quantity'      => $value['quantity'],
                'unit_price'    => $value['unit_price'],
                'total_price'   => number_format($value['quantity'] * $value['unit_price'],2,'.',''),
            );
        }

        $this->db->where('id_order', $id)->delete('app_order_product');
        if(!empty($insert_products)) {
            $this->db->insert_batch('app_order_product', $insert_products);
        }

        $update =  array(
            'id_client'     => $data['id_client'],
            'id_payment'    => $data['id_payment'],
            'weight'        => number_format($order_weight, 2,'.',''),
            'quantity'      => number_format($order_quantity, 2,'.',''),
            'price'         => number_format($order_price, 2,'.',''),
        );

        $this->db->where('id',$id)->update($this->table, $update);

        $this->db->trans_complete();

        return $this->db->trans_status() ? $this->get(array('id' => $id)) : FALSE;
    }

    public function sort($data)
    {
        $this->db->trans_start();
        $i = 1;
        foreach ($data as $value) {
            $this->db->where('id', $value)
                     ->update($this->table, array('order_by' => $i));
            $i++;
        }
        $this->db->trans_complete();
        return $this->db->trans_status();
    }

}
