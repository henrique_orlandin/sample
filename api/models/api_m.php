<?php (defined('BASEPATH')) or exit('No direct script access allowed');

/**
 * Model
 *
 * @subpackage API
 * @category Model
 * @author Henrique Orlandin
 * @copyright 2018 Henri
 */
class Api_m extends MY_Model
{
    public $auth = null;

    public function __construct() {
        $this->get_auth();
        parent::__construct();
    }

    public function get_auth ()
    {
        $this->auth = new stdClass();
        $this->auth->token = $this->input->server('HTTP_API_TOKEN') ? $this->input->server('HTTP_API_TOKEN') : null;
        $this->auth->version = $this->input->server('HTTP_API_VERSION') ? $this->input->server('HTTP_API_VERSION') : null;
        $this->auth->session_id = $this->input->server('HTTP_SESSION_ID') ? $this->input->server('HTTP_SESSION_ID') : null;
        $this->auth->origin = $this->input->server('HTTP_ORIGIN_PLATFORM') ? $this->input->server('HTTP_ORIGIN_PLATFORM') : 'app';
        $this->auth->user = null;

        if ($this->auth->session_id){
            $this->db->select('*')
                     ->from('app_session')
                     ->where('session_id', $this->auth->session_id);
            $result = $this->db->get()->row();

            if (!empty($result) && $result)
                $this->auth->user = unserialize($result->user_data);
            else
                $this->auth->session_id = null;
        }

    }

}