<?php (defined('BASEPATH')) or exit('No direct script access allowed');

require_once(__DIR__ . '/api_m.php');

/**
 * Model
 *
 * @subpackage API
 * @category Model
 * @author Henrique Orlandin
 * @copyright 2018 Henri
 */
class Loading_m extends Api_m
{

    public $table = 'app_loading';

    public function __construct()
    {
        parent::__construct();
    }

    public function get($params = array())
    {
        $options = array(
            'count'     => FALSE,
            'id'        => FALSE,
            'where'     => FALSE,
            'product_view' => FALSE,
            'limit'     => FALSE,
            'offset'    => FALSE,
        );
        $params = array_merge($options, $params);

        if ($params['count'])
            $this->db->select('COUNT(DISTINCT '.$this->table.'.id) AS count');
        else {
            $this->db->select($this->table.'.*')
                     ->select('DATE_FORMAT('.$this->table.'.date,"%d/%m/%Y") as date', FALSE)
                     ->select('WEEKDAY(date) as weekday');

            $this->db->_protect_identifiers = FALSE;
            $this->db->order_by('CASE
                                    WHEN status="pending" THEN 0
                                    WHEN status="processing" THEN 1
                                    WHEN status="completed" THEN 2
                                    WHEN status="billed" THEN 3
                                END', 'ASC', FALSE);
            $this->db->_protect_identifiers = TRUE;
            $this->db->order_by('date', 'DESC');

            if ($params['limit'] !== FALSE && $params['offset'] === FALSE)
                $this->db->limit($params['limit']);
            elseif ($params['limit'] !== FALSE)
                $this->db->limit($params['limit'], $params['offset']);
        }

        $this->db->from($this->table)
                 ->where('id_user', $this->auth->user->id);

        if ($params['id']){
            $this->db->where($this->table.'.id', $params['id']);
        }

        if ($params['where'] !== FALSE){
            if (is_array($params['where']))
                $this->db->where($params['where']);
            else
                $this->db->where($params['where'], FALSE, FALSE);
        }

        $query = $this->db->get();
        $toReturn = array();

        if ($params['count']){
            $data = $query->row();
            $toReturn = (int) $data->count;
        } else if ($params['id']){

            $data = $query->row();
            if (!$data)
                return FALSE;

            if($params['product_view']) {
                $this->db->select('sum(op.total_price) as price, sum(op.quantity) as quantity, pd.title as product')
                         ->from('app_order as o')
                         ->join('app_order_product as op', 'op.id_order = o.id', 'INNER')
                         ->join('app_product_description as pd', 'pd.id_product = op.id_product', 'INNER')
                         ->where('o.id_loading', $data->id)
                         ->group_by('op.id_product');

                $query = $this->db->get();
                $data->products = $query->result();
            } else {
                $this->db->select('o.*, c.name as client')
                         ->from('app_order as o')
                         ->join('app_client as c', 'o.id_client = c.id', 'INNER')
                         ->where('o.id_loading', $data->id);

                $query = $this->db->get();
                $data->orders = $query->result();
            }

            $toReturn = $data;

        } else {
            $toReturn = $query->result();
        }

        return $toReturn;
    }

    public function insert($data)
    {
        $this->db->trans_start();

        $company = (object)$_SESSION['company'];

        $insert =  array(
            'id_company'    => $company->id,
            'id_user'       => $this->auth->user->id,
            'date'          => !empty($data['date']) ? $data['date'] : NULL,
        );

        $this->db->insert($this->table, $insert);

        $id = $this->db->insert_id();

        $this->db->trans_complete();

        return $this->db->trans_status() ? $this->get(array('id' => $id)) : FALSE;
    }

    public function update($id)
    {
        $this->db->trans_start();

        $current = $this->get(array('id' => $id));

        if (!$id || !$current)
            return false;

        $this->db->select('sum(weight) as weight, sum(price) as price, sum(quantity) as quantity')
                 ->from('app_order')
                 ->where('id_loading',$id);
        $query = $this->db->get();
        $data = $query->row();

        $update = array(
            'weight' => $data->weight,
            'price' => $data->price,
            'quantity' => $data->quantity,
        );

        $this->db->where(array('id' => $id))->update($this->table, $update);

        $this->db->trans_complete();

        return $this->db->trans_status() ? $this->get(array('id' => $id)) : FALSE;
    }

    public function change_status ($id, $status)
    {
        $this->db->trans_start();

        $this->db->where(array('id' => $id))->update($this->table, array('status' => $status));

        $this->db->insert('app_loading_log', array(
            'admin'      => FALSE,
            'id_loading' => $id,
            'status'     => $status,
        ));

        $this->db->trans_complete();
        return $this->db->trans_status();
    }

}
